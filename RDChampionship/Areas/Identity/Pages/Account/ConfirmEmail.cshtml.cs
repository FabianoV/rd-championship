﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;


namespace RDChampionship.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class ConfirmEmailModel : PageModel
    {
        private readonly AppUserManager _userManager;

        public ConfirmEmailModel(AppUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<IActionResult> OnGetAsync(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToPage("/Index");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{userId}'.");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (!result.Succeeded)
            {
                throw new InvalidOperationException($"Error confirming email for user with ID '{userId}':");
            }

            if (!await _userManager.IsInRoleAsync(user, AppRoleService.Participant))
            {
                await _userManager.AddToRoleAsync(user, AppRoleService.Participant);
            }

            if (!await _userManager.IsInRoleAsync(user, AppRoleService.FreeMatchPlayer))
            {
                await _userManager.AddToRoleAsync(user, AppRoleService.FreeMatchPlayer);
            }

            return Page();
        }
    }
}
