﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models;
using RDChampionship.Repositories.Contract;
using RDChampionship.Repositories.Implementation;
using RDChampionship.Services.Contract;

namespace RDChampionship.Controllers
{
    public class HomeController : Controller
    {
        // Properties
        public ITournamentRepository TournamentRepository { get; set; }

        // Constructor
        public HomeController(ITournamentRepository tournamentRepository)
        {
            TournamentRepository = tournamentRepository;
        }

        // Methods
        public IActionResult Index()
        {
            var vm = new HomeIndexViewModel
            {
                Tournaments = TournamentRepository.GetAll(),
            };

            return View(vm);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
