﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Extensions;
using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;

namespace RDChampionship.Controllers
{
    public class MatchController : Controller
    {
        // Properties
        public AppUserManager UserManager { get; set; }
        public IMatchRepository MatchRepository { get; set; }
        public ISetRepository SetRepository { get; set; }
        public IPointsRepository PointsRepository { get; set; }

        // Constructor
        public MatchController(AppUserManager userManager, IMatchRepository matchRepository, ISetRepository setRepository, IPointsRepository pointsRepository)
        {
            UserManager = userManager;
            MatchRepository = matchRepository;
            SetRepository = setRepository;
            PointsRepository = pointsRepository;
        }

        // Methods
        public ActionResult Index()
        {
            var vm = new MatchIndexViewModel
            {
                Matches = MatchRepository.GetAll(),
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        public ActionResult Details(int id)
        {
            return View();
        }

        [Authorize]
        public ActionResult Create()
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.FreeMatchPlayer))
            {
                return RedirectToAction("Error", "Home");
            }

            var vm = new MatchCreateViewModel
            {
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        [HttpGet, Route("Match/ControlPanel/{matchId}")]
        public async Task<IActionResult> ControlPanel(Guid matchId)
        {
            var match = MatchRepository.Get(matchId);
            var set = SetRepository.GetCurrentSetForMatch(matchId);
            var allSets = SetRepository.GetSetsForMatch(matchId);
            var teamOnePlayerOne = await UserManager.FindByIdAsync(match.TeamOnePlayerOneId);
            var teamOnePlayerTwo = await UserManager.FindByIdAsync(match.TeamOnePlayerTwoId);
            var teamTwoPlayerOne = await UserManager.FindByIdAsync(match.TeamTwoPlayerOneId);
            var teamTwoPlayerTwo = await UserManager.FindByIdAsync(match.TeamTwoPlayerTwoId);

            var vm = new MatchControlPanelViewModel
            {
                Match = match,
                TeamOnePlayerOne = teamOnePlayerOne,
                TeamOnePlayerTwo = teamOnePlayerTwo,
                TeamTwoPlayerOne = teamTwoPlayerOne,
                TeamTwoPlayerTwo = teamTwoPlayerTwo,
            };

            if (set != null)
            {
                vm.TeamOnePoints = (int)set.TeamOnePoints;
                vm.TeamTwoPoints = (int)set.TeamTwoPoints;
            }
            vm.TeamOneSets = allSets.Count(s => s.WinnerTeam == TeamChoice.TeamOne);
            vm.TeamTwoSets = allSets.Count(s => s.WinnerTeam == TeamChoice.TeamTwo);

            return View(vm);
        }

        // --------------------------------------------------------------------------------------------------

        [HttpPost]
        public ActionResult Create(Match match)
        {
            if (!UserManager.Users.Any(user => user.Id == match.TeamOnePlayerOneId) ||
                !UserManager.Users.Any(user => user.Id == match.TeamOnePlayerTwoId) ||
                !UserManager.Users.Any(user => user.Id == match.TeamTwoPlayerOneId) ||
                !UserManager.Users.Any(user => user.Id == match.TeamTwoPlayerTwoId))
            {
                return RedirectToAction("Create");
            }

            // Is match has 4 diffrent players

            match.Created = DateTime.Now;

            MatchRepository.Create(match);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public ActionResult Delete(Guid id)
        {
            var match = MatchRepository.Get(id);
            MatchRepository.Delete(match);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult StartMatch(Guid matchId)
        {
            var match = MatchRepository.Get(matchId);
            match.WinnerTeam = TeamChoice.None;
            match.Status = MatchStatus.During;
            MatchRepository.Update(match);

            var set = new Set
            {
                Created = DateTime.Now,
                MatchId = match.Id,
                Number = 0,
                IsEnded = false,
                TeamOnePoints = 0,
                TeamTwoPoints = 0,
            };
            SetRepository.Create(set);

            return RedirectToAction(nameof(ControlPanel), new { matchId });
        }

        [HttpPost]
        public IActionResult EndMatch(Guid matchId)
        {
            var match = MatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);
            var sets = SetRepository.GetSetsForMatch(matchId);

            SummarizeAndEndSet(currentSet);
            SummarizeAndEndMatch(match, sets);
            SetRepository.Update(currentSet);
            MatchRepository.Update(match);

            return RedirectToAction($"ControlPanel", new { matchId });
        }

        [HttpPost]
        public IActionResult AddPoint(Guid matchId, string team)
        {
            var match = MatchRepository.Get(matchId);
            var set = SetRepository.GetCurrentSetForMatch(matchId);

            if (team == "one")
            {
                set.TeamOnePoints += 1;
            }
            else if (team == "two")
            {
                set.TeamTwoPoints += 1;
            }

            SetRepository.Update(set);

            return RedirectToAction($"ControlPanel", new { matchId });
        }

        [HttpPost]
        public IActionResult SubstractPoint(Guid matchId, string team)
        {
            var match = MatchRepository.Get(matchId);
            var set = SetRepository.GetCurrentSetForMatch(matchId);

            if (team == "one")
            {
                set.TeamOnePoints -= 1;
            }
            else if (team == "two")
            {
                set.TeamTwoPoints -= 1;
            }

            SetRepository.Update(set);

            return RedirectToAction($"ControlPanel", new { matchId, });
        }

        [HttpPost]
        public IActionResult NextSet(Guid matchId)
        {
            var match = MatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);
            SummarizeAndEndSet(currentSet);
            SetRepository.Update(currentSet);

            var set = new Set
            {
                Created = DateTime.Now,
                IsEnded = false,
                MatchId = matchId,
                Number = currentSet.Number + 1,
            };
            SetRepository.Create(set);

            return RedirectToAction("ControlPanel", new { matchId, });
        }

        [HttpPost]
        public IActionResult PreviousSet(Guid matchId)
        {
            var match = MatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);

            if (currentSet.Number > 0)
            {
                var previousSet = SetRepository.GetByNumber(matchId, currentSet.Number - 1);
                SetRepository.Delete(currentSet);
                previousSet.IsEnded = false;
                previousSet.WinnerTeam = TeamChoice.None;
                SetRepository.Update(previousSet);
            }

            return RedirectToAction("ControlPanel", new { matchId, });
        }

        public void SummarizeAndEndSet(Set set)
        {
            if (set.IsEnded)
            {
                throw new ArgumentException("Cannot summarize ended set.");
            }

            if (set.TeamOnePoints > set.TeamTwoPoints)
            {
                set.WinnerTeam = TeamChoice.TeamOne;
            }
            else if (set.TeamOnePoints < set.TeamTwoPoints)
            {
                set.WinnerTeam = TeamChoice.TeamTwo;
            }
            else
            {
                set.WinnerTeam = TeamChoice.TeamOneAndTeamTwo;
            }
            set.IsEnded = true;
        }

        public void SummarizeAndEndMatch(Match match, List<Set> sets)
        {
            int setsPointsTeamOne = sets.Count(s => s.WinnerTeam == TeamChoice.TeamOne);
            int setsPointsTeamTwo = sets.Count(s => s.WinnerTeam == TeamChoice.TeamTwo);

            if (setsPointsTeamOne > setsPointsTeamTwo)
            {
                match.WinnerTeam = TeamChoice.TeamOne;
            }
            else if (setsPointsTeamOne > setsPointsTeamTwo)
            {
                match.WinnerTeam = TeamChoice.TeamTwo;
            }
            else
            {
                match.WinnerTeam = TeamChoice.TeamOneAndTeamTwo;
            }

            match.SetPointsTeamOne = setsPointsTeamOne;
            match.SetPointsTeamTwo = setsPointsTeamTwo;
            match.Status = MatchStatus.Ended;
        }
    }
}