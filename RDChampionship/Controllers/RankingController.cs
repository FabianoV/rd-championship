﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;

namespace RDChampionship.Controllers
{
    public class RankingController : Controller
    {
        // Properties
        public ITeamRepository TeamRepository { get; set; }
        public ITournamentRepository TournamentRepository { get; set; }
        public IGroupRepository GroupRepository { get; set; }
        public IGroupTeamsRepository GroupTeamsRepository { get; set; }
        public ITournamentMatchRepository MatchRepository { get; set; }
        public IRegistrationRepository RegistrationRepository { get; set; }
        public IPlayersRankingService RankingCalculator { get; set; }

        // Constructor
        public RankingController(ITeamRepository teamr, ITournamentRepository tr, IGroupRepository gr, 
            IGroupTeamsRepository gtr, ITournamentMatchRepository mr, IRegistrationRepository rr, IPlayersRankingService prs)
        {
            TeamRepository = teamr;
            TournamentRepository = tr;
            GroupRepository = gr;
            GroupTeamsRepository = gtr;
            MatchRepository = mr;
            RegistrationRepository = rr;
            RankingCalculator = prs;
        }

        // Methods
        public IActionResult Index(Guid tournamentId, RankingSortKey sortKey=RankingSortKey.PointsDescending)
        {
            var vm = new PointsTableIndexViewModel();
            vm.Tournament = TournamentRepository.Get(tournamentId);
            vm.TableData = RankingCalculator.CalculateRanking(vm.Tournament.Id, sortKey);

            return View(vm);
        }

        public IActionResult Groups(Guid tournamentId)
        {
            var vm = new EliminationTableGroupsViewModel
            {
                Groups = GroupRepository.GetActiveGroupsForTournament(tournamentId),
            };

            return View(vm);
        }

        public IActionResult GroupTable(Guid tournamentId, Guid groupId)
        {
            var vm = new EliminationTableGroupTableViewModel
            {
                Tournament = TournamentRepository.Get(tournamentId),
                Group = GroupRepository.Get(groupId),
                // Teams = TeamRepository.GetRegisteredTeams(tournamentId),
                // Matches = MatchRepository.GetActiveMatchesForGroup(tournamentId, groupId),
            };

            return View(vm);
        }

        public IActionResult Administration(Guid tournamentId)
        {
            var vm = new EliminationTableAdministrationViewModel
            {
                Tournament = TournamentRepository.Get(tournamentId),
                // RegisteredTeams = TeamRepository.GetRegisteredTeams(tournamentId),
                Groups = GroupRepository.GetActiveGroupsForTournament(tournamentId),
                Matches = new List<TournamentMatch>(),
            };

            foreach (var group in vm.Groups)
            {
                // var matches = MatchRepository.GetActiveMatchesForGroup(tournamentId, group.Id);
                // vm.Matches.AddRange(matches);
            }

            return View(vm);
        }

        // --------------------------------------------------------------------------------------------------

        [HttpPost]
        public IActionResult RandomizeGroups(Guid tournamentId, int groupsCount)
        {
            // var tournamentSettings = MatchRandomizer.RandomMatches(tournamentId, groupsCount);

            var tournament = TournamentRepository.Get(tournamentId);
            tournament.Phase = TournamentPhase.Championship;
            TournamentRepository.Update(tournament);

            return RedirectToAction($"MatchesAdministration", new { tournamentId });
        }
    }
}