﻿using Microsoft.AspNetCore.Mvc;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Controllers
{
    public class RolesController : Controller
    {
        // Properties
        public AppUserManager UserManager { get; set; }
        public AppRoleManager RoleManager { get; set; }

        // Constructor
        public RolesController(AppUserManager userManager, AppRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        // Methods
        public async Task<List<AppRole>> GetRoles(string userId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            var userRoles = await UserManager.GetRolesAsync(user);
            var result = new List<AppRole>();
            foreach (var roleName in userRoles)
            {
                var role = await RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    continue;
                }
                result.Add(role);
            }
            return result;
        }

        [HttpPost]
        public async Task OverrideRoles(string userId, List<string> roleIdList)
        {
            if (!User.IsInRole(AppRoleService.Administrator))
            {
                return;
            }

            var user = await UserManager.FindByIdAsync(userId);

            // Remove all roles from user
            var userRolesToRemove = await UserManager.GetRolesAsync(user);
            await UserManager.RemoveFromRolesAsync(user, userRolesToRemove);

            // Assign new roles 
            foreach (var roleId in roleIdList)
            {
                var role = await RoleManager.FindByIdAsync(roleId);
                await UserManager.AddToRoleAsync(user, role.Name);
            }
        }

        [HttpPost]
        public async Task<bool> EnableRole(string userId, string roleId)
        {
            if (!User.IsInRole(AppRoleService.Administrator))
            {
                return false;
            }

            var user = await UserManager.FindByIdAsync(userId);
            var role = await RoleManager.FindByIdAsync(roleId);
            var isUserInRole = await UserManager.IsInRoleAsync(user, role.Name);
            if (!isUserInRole)
            {
                await UserManager.AddToRoleAsync(user, role.Name);
                return true;
            }
            return false;
        }

        [HttpPost]
        public async Task<bool> DisableRole(string userId, string roleId)
        {
            if (!User.IsInRole(AppRoleService.Administrator))
            {
                return false;
            }

            var user = await UserManager.FindByIdAsync(userId);
            var role = await RoleManager.FindByIdAsync(roleId);
            var isUserInRole = await UserManager.IsInRoleAsync(user, role.Name);
            if (isUserInRole)
            {
                await UserManager.RemoveFromRoleAsync(user, role.Name);
                return true;
            }
            return false;
        }
    }
}
