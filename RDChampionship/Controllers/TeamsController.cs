﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Data;
using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Identity;

namespace RDChampionship.Controllers
{
    public class TeamsController : Controller
    {
        // Services
        public AppUserManager UserManager { get; set; }
        public ITeamRepository TeamRepository { get; set; }
        public IInvitationRepository InvitationRepository { get; set; }

        // Constructor
        public TeamsController(AppUserManager userManager, ITeamRepository teamRepository, IInvitationRepository invitationRepository)
        {
            UserManager = userManager;
            TeamRepository = teamRepository;
            InvitationRepository = invitationRepository;
        }

        // Methods
        public IActionResult Index()
        {
            string userName = User.Identity.Name;
            if (TeamRepository.IsUserOwnerOfTeam(userName))
            {
                return RedirectToAction("TeamOwnerView", "Teams");
            }

            if (TeamRepository.IsUserMemeberOfTeam(userName))
            {
                return RedirectToAction("TeamMemberView", "Teams");
            }

            var vm = new TeamIndexViewModel();
            vm.UserReceivedInvitations = InvitationRepository.GetUserReceivedInvtiations(userName);
            return View(vm);
        }

        [Authorize]
        public IActionResult MyTeamsList()
        {
            var teams = TeamRepository.GetTeamsByParticipantUserName(User.Identity.Name);

            var vm = new TeamMyTeamListViewModel
            {
                LoggedUserTeams = teams,
            };

            return View();
        }

        public IActionResult TeamList()
        {
            var vm = new TeamListViewModel
            {
                Teams = TeamRepository.GetAll(),
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        [Route("Team/Details/{teamName}")]
        public async Task<IActionResult> Details(string teamName)
        {
            var team = TeamRepository.Get(teamName);
            var owner = await UserManager.FindByIdAsync(team.TeamOwnerId);
            var member = await UserManager.FindByIdAsync(team.TeamMemberId);

            var vm = new TeamDetailsViewModel
            {
                Team = team,
                TeamOwner = owner,
                TeamMember = member,
            };

            return View(vm);
        }

        //public IActionResult TeamOwnerView()
        //{
        //    var vm = new TeamOwnerViewModel
        //    {
        //        UserSendedInvitations = InvitationRepository.GetUserSendedInvitations(User.Identity.Name),
        //        UserReceivedInvitations = InvitationRepository.GetUserReceivedInvtiations(User.Identity.Name),
        //        UserTeam = TeamRepository.GetTeamByParticipantUserName(User.Identity.Name),
        //    };
        //    return View(vm);
        //}

        //public IActionResult TeamMemberView()
        //{
        //    var vm = new TeamMemberViewModel
        //    {
        //        UserReceivedInvitations = InvitationRepository.GetUserReceivedInvtiations(User.Identity.Name),
        //        UserTeam = TeamRepository.GetTeamByParticipantUserName(User.Identity.Name),
        //    };
        //    vm.TeamOwner = UserRepository.GetUserById(vm.UserTeam.TeamOwnerId);
        //    return View(vm);
        //}

        public IActionResult CreateTeamForm()
        {
            return View();
        }

        public IActionResult LeaveTeam()
        {
            var userName = User.Identity.Name;

            if (TeamRepository.IsUserOwnerOfTeam(userName))
            {
                var team = TeamRepository.GetByMemberName(userName);
                TeamRepository.Delete(team);
                InvitationRepository.DeleteSendedInvitationsByUser(userName);
            }

            if (TeamRepository.IsUserMemeberOfTeam(userName))
            {
                var team = TeamRepository.GetByMemberName(userName);
                team.TeamMemberId = null;
                TeamRepository.Update(team);
            }

            return RedirectToAction("Index", "Teams");
        }

        public IActionResult SendInvitation()
        {
            var vm = new TeamSendInvitationViewModel
            {
                Users = UserManager.Users.ToList(),
            };
            return View(vm);
        }

        //[HttpPost]
        //public IActionResult CreateTeam(string name, string logoUrl)
        //{
        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        throw new Exception("Cannot create team!");
        //    }

        //    // If team exists do nothing!
        //    var userTeam = TeamRepository.GetTeamByParticipantUserName(User.Identity.Name);

        //    if (userTeam != null)
        //    {
        //        return RedirectToAction("Index", "Teams");
        //    }

        //    var user = UserRepository.GetUserByName(User.Identity.Name);

        //    // Create Team
        //    Team team = new Team
        //    {
        //        TeamOwnerId = user.Id,
        //        Name = name,
        //        LogoUrl = logoUrl,
        //    };
        //    TeamRepository.CreateTeam(team);

        //    return RedirectToAction("Index", "Teams");
        //}

        [HttpPost]
        public async Task<IActionResult> SendInvitation(string invitedUserId)
        {
            // TODO: Sprawdź czy istnieje zaproszenie

            var invitingUser = await UserManager.FindByNameAsync(User.Identity.Name);
            var invitedUser = await UserManager.FindByIdAsync(invitedUserId);

            var invitation = new Invitation()
            {
                InvitationTime = DateTime.Now,
                InvitingPerson = invitingUser.UserName,
                InvitingPersonId = invitedUser.Id,
                InvitedPreson = invitedUser.UserName,
                InvitedPresonId = invitedUser.Id,
            };
            InvitationRepository.Create(invitation);
            return RedirectToAction("Index", "Teams");
        }

        [HttpPost]
        public IActionResult DiscardInvitation(Guid invitationId)
        {
            // TODO: Cancel all similar invitations

            return CancelInvitation(invitationId);
        }

        [HttpPost]
        public IActionResult AcceptInvitation(Guid invitationId)
        {
            var invitation = InvitationRepository.Get(invitationId);
            var team = TeamRepository.GetByOwnerName(invitation.InvitingPerson);
            team.TeamMemberId = invitation.InvitedPreson;
            TeamRepository.Update(team);
            InvitationRepository.Delete(invitation);
            return RedirectToAction("Index", "Teams");
        }

        [HttpPost]
        public IActionResult CancelInvitation(Guid invitationId)
        {
            var invitation = InvitationRepository.Get(invitationId);
            InvitationRepository.Delete(invitation);
            return RedirectToAction("Index", "Teams");
        }
    }
}