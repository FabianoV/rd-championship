﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Extensions;
using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;

namespace RDChampionship.Controllers
{
    public class TournamentController : Controller
    {
        // Services
        public AppUserManager UserManager { get; set; }
        public ITournamentRepository TournamentRepository { get; set; }
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }
        public IRegistrationRepository RegistrationRepository { get; set; }

        // Constructor
        public TournamentController(AppUserManager um, ITournamentRepository tr, IRegistrationRepository rr, ITournamentMatchRepository tmr)
        {
            UserManager = um;
            TournamentRepository = tr;
            RegistrationRepository = rr;
            TournamentMatchRepository = tmr;
        }

        // Methods
        public IActionResult Index()
        {
            var vm = new TournamentIndexViewModel
            {
                Tournaments = TournamentRepository.GetAll(),
            };

            return View(vm);
        }

        public IActionResult RegisteredUsers(Guid tournamentId)
        {
            var vm = new TournamentRegisteredUsersViewModel
            {
                TournamentId = tournamentId,
                Registrations = RegistrationRepository.GetAll(tournamentId),
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        [Authorize]
        public IActionResult Create()
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Home", "Error");
            }

            return View();
        }

        [Authorize, Route("/Tournament/Edit/{tournamentId}")]
        public IActionResult Edit(Guid tournamentId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Home", "Error");
            }

            var tournament = TournamentRepository.Get(tournamentId);
            return View(tournament);
        }

        /// <summary> Provides functionality of sign in another users by admins or moderators. </summary>
        [Authorize]
        public IActionResult SignUpUser(Guid tournamentId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Home", "Error");
            }

            var vm = new TournamentSignUpUserViewModel()
            {
                TournamentId = tournamentId,
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        [Authorize]
        public IActionResult SignInProcessedSuccesfully()
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Home", "Error");
            }

            var vm = new TournamentSignInProcessedSuccesfullyViewModel();
            return View(vm);
        }

        [Authorize]
        public IActionResult UserAlredySignedIn()
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Home", "Error");
            }

            return View();
        }

        [Authorize]
        public IActionResult UserSignedOut()
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Home", "Error");
            }

            return View();
        }

        // --------------------------------------------------------------------------------------------------

        [HttpPost, Authorize]
        public IActionResult Create(Tournament tournament)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator))
            {
                return RedirectToAction("Home", "Error");
            }

            // TODO: Validate Tournament
            TournamentRepository.Create(tournament);
            return RedirectToAction("Index");
        }

        [HttpPost, Authorize]
        public IActionResult Edit(Tournament tournament)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Error", "Home");
            }

            // TODO: Validate Tournament
            TournamentRepository.Update(tournament);
            return RedirectToAction("Index");
        }

        [HttpPost, Authorize]
        public IActionResult Delete(Guid tournamentId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator))
            {
                return RedirectToAction("Error", "Home");
            }

            var tournament = TournamentRepository.Get(tournamentId);
            TournamentRepository.Delete(tournament);
            return RedirectToAction(nameof(Index));
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> UserSignIn(Guid tournamentId, string username)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Home", "Error");
            }

            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator) && !string.IsNullOrWhiteSpace(username))
            {
                return RedirectToAction("Home", "Error"); // Another user cannot be signed in tournament by other roles than admin or moderator.
            }

            string usernameToSignIn = User.Identity.Name; 
            if (!string.IsNullOrWhiteSpace(username))
            {
                usernameToSignIn = username;
            }
            var user = await UserManager.FindByNameAsync(usernameToSignIn);
            var tournament = TournamentRepository.Get(tournamentId);

            if (!RegistrationRepository.IsUserRegisteredForTournament(user.Id, tournamentId))
            {
                var registration = new RegistrationForTournament
                {
                    UserId = user.Id,
                    TournamentId = tournament.Id
                };

                RegistrationRepository.Create(registration);
            }
            else
            {
                return RedirectToAction(nameof(UserAlredySignedIn));
            }

            return RedirectToAction(nameof(RegisteredUsers), new { tournamentId });
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> UserSignOut(Guid tournamentId, string username)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator) && !string.IsNullOrWhiteSpace(username))
            {
                return RedirectToAction("Home", "Error"); // Another user cannot be signed in tournament by other roles than admin or moderator.
            }

            string usernameToSignOut = User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(username))
            {
                usernameToSignOut = username;
            }
            var user = await UserManager.FindByNameAsync(usernameToSignOut);

            if (RegistrationRepository.IsUserRegisteredForTournament(user.Id, tournamentId))
            {
                RegistrationRepository.Delete(user.Id, tournamentId);
            }

            return RedirectToAction(nameof(RegisteredUsers), new { tournamentId });
        }
    }
}