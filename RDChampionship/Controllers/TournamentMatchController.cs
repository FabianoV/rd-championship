﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Extensions;
using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;

namespace RDChampionship.Controllers
{
    public class TournamentMatchController : Controller
    {
        // Properties
        public AppUserManager UserManager { get; set; }
        public ITournamentRepository TournamentRepository { get; set; }
        public IGroupRepository GroupRepository { get; set; }
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }
        public ITeamRepository TeamRepository { get; set; }
        public IMatchRepository MatchRepository { get; set; }
        public ITournamentMatchControlService TournamentMatchControlService { get; set; }
        public IRegistrationRepository RegistrationRepository { get; set; }
        public IUserStatisticsService UserStatisticsService { get; set; }

        // Constructor
        public TournamentMatchController(AppUserManager userManager, ITournamentRepository tournamentRepository, IGroupRepository groupRepository,
            ITeamRepository teamRepository, ITournamentMatchRepository tournamentMatchRepository, IRegistrationRepository registrationRepository, 
            IMatchRepository matchRepository, ITournamentMatchControlService tournamentMatchControlService, IUserStatisticsService userStatisticsService)
        {
            UserManager = userManager;
            TournamentRepository = tournamentRepository;
            GroupRepository = groupRepository;
            TeamRepository = teamRepository;
            TournamentMatchRepository = tournamentMatchRepository;
            MatchRepository = matchRepository;
            RegistrationRepository = registrationRepository;

            TournamentMatchControlService = tournamentMatchControlService;
            UserStatisticsService = userStatisticsService;
        }

        // Methods
        public IActionResult Index(Guid tournamentId)
        {
            var vm = new TournamentMatchIndexViewModel
            {
                Tournament = TournamentRepository.Get(tournamentId),
                Matches = TournamentMatchRepository.GetAll(tournamentId),
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        public IActionResult List(Guid tournamentId)
        {
            return RedirectToAction(nameof(Index), new { tournamentId });
        }

        [Authorize]
        public IActionResult MatchesAdministration(Guid tournamentId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Home", "Error");
            }

            var vm = new TournamentMatchesAdministrationViewModel
            {
                Tournament = TournamentRepository.Get(tournamentId),
                Matches = TournamentMatchRepository.GetAll(tournamentId),
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        public async Task<IActionResult> ControlPanel(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var vm = new TournamentMatchControlPanelViewModel
            {
                Match = match,
                MatchScore = TournamentMatchControlService.GetCurrentMatchScore(matchId),
            };

            vm.TeamOnePlayerOne = await UserManager.FindByIdAsync(match.TeamOnePlayerOneId);
            vm.TeamOnePlayerTwo = await UserManager.FindByIdAsync(match.TeamOnePlayerTwoId);
            vm.TeamTwoPlayerOne = await UserManager.FindByIdAsync(match.TeamTwoPlayerOneId);
            vm.TeamTwoPlayerTwo = await UserManager.FindByIdAsync(match.TeamTwoPlayerTwoId);

            string username = User.Identity.Name;
            if (vm.TeamOnePlayerOne.UserName == username || vm.TeamOnePlayerTwo.UserName == username ||
                vm.TeamTwoPlayerOne.UserName == username || vm.TeamTwoPlayerTwo.UserName == username)
            {
                vm.IsCurrentUserIsMatchPlayer = true;
            }
            else
            {
                vm.IsCurrentUserIsMatchPlayer = false;
            }

            return View(vm);
        }

        [Authorize]
        public IActionResult Create(Guid tournamentId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Home", "Error");
            }

            var vm = new TournamentMatchCreateViewModel
            {
                TournamentMatch = new TournamentMatch
                {
                    TournamentId = tournamentId,
                    Name = "Mecz turniejowy",
                    StartTime = DateTime.Now.Date,
                    EndTime = DateTime.Now.AddDays(14).Date,
                },
            };

            // Select torunament participants
            vm.TournamentParticipants = new List<AppUser>();
            foreach (var user in UserManager.Users)
            {
                var registration = RegistrationRepository.Get(user.Id, tournamentId);
                if (registration != null)
                {
                    vm.TournamentParticipants.Add(user);
                }
            }

            return View(vm);
        }

        public IActionResult Edit(Guid matchId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Error", "Home");
            }

            var match = TournamentMatchRepository.Get(matchId);

            var vm = new TournamentMatchEditViewModel()
            {
                TournamentMatch = match,
            };

            // Select torunament participants
            vm.TournamentParticipants = new List<AppUser>();
            foreach (var user in UserManager.Users)
            {
                var registration = RegistrationRepository.Get(user.Id, match.TournamentId);
                if (registration != null)
                {
                    vm.TournamentParticipants.Add(user);
                }
            }

            return View(vm);
        }

        // --------------------------------------------------------------------------------------------------

        [HttpPost, Authorize]
        public IActionResult Create(TournamentMatch match)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Error", "Home");
            }

            match.Created = DateTime.Now;
            TournamentMatchRepository.Create(match);
            return RedirectToAction(nameof(MatchesAdministration), new { tournamentId = match.TournamentId });
        }

        [HttpPost, Authorize]
        public IActionResult Edit(TournamentMatch match, Guid matchId)
        {
            var matchDb = TournamentMatchRepository.Get(matchId);

            matchDb.Name = match.Name;
            matchDb.Status = match.Status;
            matchDb.WinnerTeam = match.WinnerTeam;
            matchDb.IsActive = match.IsActive;
            matchDb.SetPointsTeamOne = match.SetPointsTeamOne;
            matchDb.SetPointsTeamTwo = match.SetPointsTeamTwo;
            matchDb.TeamOnePlayerOneId = match.TeamOnePlayerOneId;
            matchDb.TeamOnePlayerTwoId = match.TeamOnePlayerTwoId;
            matchDb.TeamTwoPlayerOneId = match.TeamTwoPlayerOneId;
            matchDb.TeamTwoPlayerTwoId = match.TeamTwoPlayerTwoId;

            TournamentMatchRepository.Update(matchDb);

            return RedirectToAction(nameof(Index), new { tournamentId = matchDb.TournamentId });
        }

        [HttpPost, Authorize]
        public IActionResult Delete(Guid matchId)
        {
            if (!User.IsInRole(AppRoleService.Administrator))
            {
                return RedirectToAction("Error", "Home");
            }

            var match = TournamentMatchRepository.Get(matchId);
            TournamentMatchRepository.Delete(match);
            return RedirectToAction(nameof(MatchesAdministration), new { match.TournamentId });
        }

        [HttpPost]
        public IActionResult StartMatch(Guid matchId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.StartMatch(matchId);

            return RedirectToAction($"ControlPanel", new { matchId });
        }

        [HttpPost]
        public IActionResult EndMatch(Guid matchId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.EndMatch(matchId);
            

            UserStatisticsService.SumStatisticsForTournamentMatch(UserManager.FindByNameAsync(User.Identity.Name).Result.Id, matchId);

            return RedirectToAction($"ControlPanel", new { matchId });
        }

        [HttpPost]
        public IActionResult AddPoint(Guid matchId, string team)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.AddPoint(matchId, team);

            return RedirectToAction($"ControlPanel", new { matchId });
        }

        [HttpPost]
        public IActionResult SubstractPoint(Guid matchId, string team)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.SubstractPoint(matchId, team);

            return RedirectToAction($"ControlPanel", new { matchId, });
        }

        [HttpPost]
        public IActionResult NextSet(Guid matchId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.NextSet(matchId);

            return RedirectToAction("ControlPanel", new { matchId, });
        }

        [HttpPost]
        public IActionResult PreviousSet(Guid matchId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator, AppRoleService.Participant))
            {
                return RedirectToAction("Error", "Home");
            }

            TournamentMatchControlService.PreviousSet(matchId);

            return RedirectToAction("ControlPanel", new { matchId, });
        }
    }
}