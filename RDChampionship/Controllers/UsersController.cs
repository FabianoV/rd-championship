﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RDChampionship.Extensions;
using RDChampionship.Models;
using RDChampionship.Services.Identity;
using RDChampionship.Services.Implementation;

namespace RDChampionship.Controllers
{
    public class UsersController : Controller
    {
        // Services
        public AppUserManager UserManager { get; set; }

        // Constructor
        public UsersController(AppUserManager userManager)
        {
            UserManager = userManager;
        }

        // Methods
        public async Task<IActionResult> Details(string id)
        {
            var vm = new UserDetailsViewModel
            {
                User = await UserManager.FindByIdAsync(id),
            };

            return View(vm);
        }

        public IActionResult List()
        {
            var vm = new UserListViewModel
            {
                Users = UserManager.Users.ToList(),
            };

            return View(vm);
        }

        public async Task<IActionResult> Edit(string userId)
        {
            if (!User.IsAtLeastInOneRole(AppRoleService.Administrator, AppRoleService.Moderator))
            {
                return RedirectToAction("Error", "Home");
            }

            var user = await UserManager.FindByIdAsync(userId);

            if (user == null)
            {
                return RedirectToAction("Error", "Home");
            }

            return View(user);
        }

        [Authorize]
        public IActionResult Create()
        {
            if (!User.IsInRole(AppRoleService.Administrator))
            {
                return RedirectToAction("Error", "Home");
            }

            return View();
        }

        // --------------------------------------------------------------------------------------------------

        [HttpPost, Authorize]
        public async Task<IActionResult> Edit(AppUser user)
        {
            var userBeforeChange = await UserManager.FindByIdAsync(user.Id);

            if (string.IsNullOrWhiteSpace(user.UserName))
            {
                return RedirectToAction("Error", "Home"); // User should have not null or empty username
            }

            if (string.IsNullOrWhiteSpace(user.Email))
            {
                return RedirectToAction("Error", "Home"); // User should have not null or empty email
            }

            var u = await UserManager.FindByEmailAsync(user.Email);
            if (u != null)
            {
                return RedirectToAction("Error", "Home"); // User should have unique email
            }

            // Fields possible to update
            userBeforeChange.Email = user.Email;
            userBeforeChange.UserName = user.UserName;
            userBeforeChange.AvatarUrl = user.AvatarUrl;
            userBeforeChange.EmailConfirmed = user.EmailConfirmed;

            await UserManager.UpdateAsync(userBeforeChange);

            return RedirectToAction();
        }
    }
}