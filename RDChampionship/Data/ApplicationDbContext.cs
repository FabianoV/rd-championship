﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;

namespace RDChampionship.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, string, IdentityUserClaim<string>, 
        IdentityUserRole<string>, IdentityUserLogin<string>, IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        // Properties
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Tournament> Tournaments { get; set; }
        public DbSet<RegistrationForTournament> RegistrationForTournaments { get; set; }
        public DbSet<Match> Matches { get; set; }
        public DbSet<TournamentMatch> TournamentMatches { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<GroupTeam> ScheduleGroupTeams { get; set; }
        public DbSet<Point> Points { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<UserStatistics> UserStatistics { get; set; }

        // Constructor
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
