﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using RDChampionship.Services.Identity;

namespace RDChampionship.Extensions
{
    public static class ClaimsPrincipalExtension
    {
        public static bool IsAtLeastInOneRole(this ClaimsPrincipal user, params string[] roles)
        {
            bool hasAccess = false;

            foreach (string role in roles)
            {
                if (user.IsInRole(role))
                {
                    hasAccess = true;
                    break;
                }
            }

            return hasAccess;
        }

        public static bool IsInAllRoles(this ClaimsPrincipal user, params string[] roles)
        {
            bool hasAccess = true;

            foreach (string role in roles)
            {
                if (!user.IsInRole(role))
                {
                    hasAccess = false;
                    break;
                }
            }

            return hasAccess;
        }

        public static bool HasEmailConfirmed(this ClaimsPrincipal principal, AppUserManager userManager)
        {
            var appUser = userManager.GetUserAsync(principal).Result;
            var isEmailConfirmed = userManager.IsEmailConfirmedAsync(appUser).Result;
            return isEmailConfirmed;
        }
    }
}
