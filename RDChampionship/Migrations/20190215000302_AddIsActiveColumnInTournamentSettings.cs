﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class AddIsActiveColumnInTournamentSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "TournamentSettings",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "GroupId",
                table: "ScheduleGroupTeams",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "GroupName",
                table: "ScheduleGroupTeams",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "TournamentSettings");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "ScheduleGroupTeams");

            migrationBuilder.DropColumn(
                name: "GroupName",
                table: "ScheduleGroupTeams");
        }
    }
}
