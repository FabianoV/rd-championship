﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class AddIsAvtiveFieldToScheduleGroupTeamTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "ScheduleGroupTeams",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "ScheduleGroupTeams");
        }
    }
}
