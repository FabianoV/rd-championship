﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class AddScheduleMatchTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScheduleMatches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TournamentName = table.Column<string>(nullable: true),
                    GroupId = table.Column<Guid>(nullable: false),
                    GroupName = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    TeamOneId = table.Column<Guid>(nullable: false),
                    TeamTwoId = table.Column<Guid>(nullable: false),
                    TeamOneName = table.Column<string>(nullable: true),
                    TeamTwoName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleMatches", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduleMatches");
        }
    }
}
