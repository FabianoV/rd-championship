﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class UpdatedSetAndPointTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegistrationTime",
                table: "Points",
                newName: "Created");

            migrationBuilder.AddColumn<string>(
                name: "OwnerTeamName",
                table: "Points",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SetId",
                table: "Points",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "Sets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    MatchId = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsEnded = table.Column<bool>(nullable: false),
                    WinnerTeamName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sets", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sets");

            migrationBuilder.DropColumn(
                name: "OwnerTeamName",
                table: "Points");

            migrationBuilder.DropColumn(
                name: "SetId",
                table: "Points");

            migrationBuilder.RenameColumn(
                name: "Created",
                table: "Points",
                newName: "RegistrationTime");
        }
    }
}
