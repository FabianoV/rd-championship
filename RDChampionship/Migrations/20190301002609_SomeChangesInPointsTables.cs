﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class SomeChangesInPointsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RegistrationTime",
                table: "Matches",
                newName: "Started");

            migrationBuilder.AddColumn<int>(
                name: "Number",
                table: "Sets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "TeamOnePoints",
                table: "Sets",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TeamTwoPoints",
                table: "Sets",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Matches",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Number",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "TeamOnePoints",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "TeamTwoPoints",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "Started",
                table: "Matches",
                newName: "RegistrationTime");
        }
    }
}
