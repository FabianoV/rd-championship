﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class MergedScheduleMatchWithMatch : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ScheduleMatches");

            migrationBuilder.RenameColumn(
                name: "Started",
                table: "Matches",
                newName: "StartTime");

            migrationBuilder.RenameColumn(
                name: "MatchScheduleId",
                table: "Matches",
                newName: "TeamTwoId");

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Matches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "GroupId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "GroupName",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "TeamOneId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "GroupName",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamOneId",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "TeamTwoId",
                table: "Matches",
                newName: "MatchScheduleId");

            migrationBuilder.RenameColumn(
                name: "StartTime",
                table: "Matches",
                newName: "Started");

            migrationBuilder.CreateTable(
                name: "ScheduleMatches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    GroupName = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    TeamOneId = table.Column<Guid>(nullable: false),
                    TeamOneName = table.Column<string>(nullable: true),
                    TeamTwoId = table.Column<Guid>(nullable: false),
                    TeamTwoName = table.Column<string>(nullable: true),
                    TournamentName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheduleMatches", x => x.Id);
                });
        }
    }
}
