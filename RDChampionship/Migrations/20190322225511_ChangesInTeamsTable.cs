﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class ChangesInTeamsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WinnerTeamName",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "GroupName",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamOneName",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamTwoName",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "TeamOwner",
                table: "Teams",
                newName: "TeamOwnerId");

            migrationBuilder.RenameColumn(
                name: "TeamMember",
                table: "Teams",
                newName: "TeamMemberId");

            migrationBuilder.RenameColumn(
                name: "TournamentName",
                table: "Matches",
                newName: "Name");

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerTeamId",
                table: "Sets",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TournamentId",
                table: "RegistrationForTournaments",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<bool>(
                name: "IsTournamentMatch",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "TournamentId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    AvatarUrl = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropColumn(
                name: "WinnerTeamId",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "TournamentId",
                table: "RegistrationForTournaments");

            migrationBuilder.DropColumn(
                name: "IsTournamentMatch",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TournamentId",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "TeamOwnerId",
                table: "Teams",
                newName: "TeamOwner");

            migrationBuilder.RenameColumn(
                name: "TeamMemberId",
                table: "Teams",
                newName: "TeamMember");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Matches",
                newName: "TournamentName");

            migrationBuilder.AddColumn<string>(
                name: "WinnerTeamName",
                table: "Sets",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "GroupName",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamOneName",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamTwoName",
                table: "Matches",
                nullable: true);
        }
    }
}
