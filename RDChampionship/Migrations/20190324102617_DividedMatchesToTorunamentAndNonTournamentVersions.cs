﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class DividedMatchesToTorunamentAndNonTournamentVersions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsTournamentMatch",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamOneId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamTwoId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TournamentId",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "TeamTwoColor",
                table: "Matches",
                newName: "TeamTwoPlayerTwoId");

            migrationBuilder.RenameColumn(
                name: "TeamOneColor",
                table: "Matches",
                newName: "TeamTwoPlayerOneId");

            migrationBuilder.AddColumn<string>(
                name: "TeamOnePlayerOneId",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamOnePlayerTwoId",
                table: "Matches",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TournamentMatches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    TournamentId = table.Column<Guid>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TeamOneId = table.Column<Guid>(nullable: false),
                    TeamTwoId = table.Column<Guid>(nullable: false),
                    TeamOneColor = table.Column<string>(nullable: true),
                    TeamTwoColor = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentMatches", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TournamentMatches");

            migrationBuilder.DropColumn(
                name: "TeamOnePlayerOneId",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TeamOnePlayerTwoId",
                table: "Matches");

            migrationBuilder.RenameColumn(
                name: "TeamTwoPlayerTwoId",
                table: "Matches",
                newName: "TeamTwoColor");

            migrationBuilder.RenameColumn(
                name: "TeamTwoPlayerOneId",
                table: "Matches",
                newName: "TeamOneColor");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "Matches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "GroupId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsTournamentMatch",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Matches",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "TeamOneId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TeamTwoId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TournamentId",
                table: "Matches",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
