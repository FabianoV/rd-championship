﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class SchemaChangeForNormalNonTournamentMatches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WinnerTeamId",
                table: "Sets");

            migrationBuilder.AddColumn<int>(
                name: "WinnerTeam",
                table: "Sets",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SetPointsTeamOne",
                table: "Matches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SetPointsTeamTwo",
                table: "Matches",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WinnerTeam",
                table: "Matches",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WinnerTeam",
                table: "Sets");

            migrationBuilder.DropColumn(
                name: "SetPointsTeamOne",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "SetPointsTeamTwo",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "WinnerTeam",
                table: "Matches");

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerTeamId",
                table: "Sets",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }
    }
}
