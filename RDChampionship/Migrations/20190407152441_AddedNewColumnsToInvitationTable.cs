﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class AddedNewColumnsToInvitationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "InvitedPresonId",
                table: "Invitations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InvitingPersonId",
                table: "Invitations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "InvitingTeamId",
                table: "Invitations",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InvitedPresonId",
                table: "Invitations");

            migrationBuilder.DropColumn(
                name: "InvitingPersonId",
                table: "Invitations");

            migrationBuilder.DropColumn(
                name: "InvitingTeamId",
                table: "Invitations");
        }
    }
}
