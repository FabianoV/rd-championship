﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class RemoveTournamentSettingsAndRedundantData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TournamentMatches");

            migrationBuilder.DropTable(
                name: "TournamentSettings");

            migrationBuilder.DropColumn(
                name: "GroupName",
                table: "ScheduleGroupTeams");

            migrationBuilder.DropColumn(
                name: "TeamName",
                table: "ScheduleGroupTeams");

            migrationBuilder.DropColumn(
                name: "TournamentName",
                table: "ScheduleGroupTeams");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Matches",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Matches",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "TournamentId",
                table: "Matches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "TournamentId",
                table: "Matches");

            migrationBuilder.AddColumn<string>(
                name: "GroupName",
                table: "ScheduleGroupTeams",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamName",
                table: "ScheduleGroupTeams",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TournamentName",
                table: "ScheduleGroupTeams",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TournamentMatches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    GroupId = table.Column<Guid>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    TeamOneColor = table.Column<string>(nullable: true),
                    TeamOneId = table.Column<Guid>(nullable: false),
                    TeamTwoColor = table.Column<string>(nullable: true),
                    TeamTwoId = table.Column<Guid>(nullable: false),
                    TournamentId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentMatches", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TournamentSettings",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TournamentId = table.Column<Guid>(nullable: false),
                    TournamentName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TournamentSettings", x => x.Id);
                });
        }
    }
}
