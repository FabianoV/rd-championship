﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RDChampionship.Migrations
{
    public partial class ChangedWayOfSigningInOnTournamentInSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TeamName",
                table: "RegistrationForTournaments");

            migrationBuilder.RenameColumn(
                name: "TournamentName",
                table: "RegistrationForTournaments",
                newName: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "RegistrationForTournaments",
                newName: "TournamentName");

            migrationBuilder.AddColumn<string>(
                name: "TeamName",
                table: "RegistrationForTournaments",
                nullable: true);
        }
    }
}
