﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class EliminationTableAdministrationViewModel
    {
        public Tournament Tournament { get; set; }
        public List<Team> RegisteredTeams { get; set; }
        public List<Group> Groups { get; set; }
        public List<GroupTeam> GroupsMembers { get; internal set; }
        public List<TournamentMatch> Matches { get; set; }
        public List<AppUser> Users { get; set; }
    }
}
