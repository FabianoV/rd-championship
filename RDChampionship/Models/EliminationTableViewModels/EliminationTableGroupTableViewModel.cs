﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class EliminationTableGroupTableViewModel
    {
        public Tournament Tournament { get; set; }
        public Group Group { get; set; }
        public List<Team> Teams { get; set; }
        public List<TournamentMatch> Matches { get; set; }
    }
}
