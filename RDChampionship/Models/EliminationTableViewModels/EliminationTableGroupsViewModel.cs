﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class EliminationTableGroupsViewModel
    {
        public List<Group> Groups { get; set; }
    }
}
