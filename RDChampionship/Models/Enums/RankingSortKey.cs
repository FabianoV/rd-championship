﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public enum RankingSortKey
    {
        PositionAscending,
        PositionDescending,
        PlayerNameAscending,
        PlayerDescending,
        PointsAscending,
        PointsDescending,
        PlayedMatchesAscending,
        PlayedMatchesDescending
    }
}
