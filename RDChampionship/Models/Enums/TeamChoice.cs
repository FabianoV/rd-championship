﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public enum TeamChoice
    {
        None = 0,
        TeamOne = 1,
        TeamTwo = 2,
        TeamOneAndTeamTwo = 3,
    }
}
