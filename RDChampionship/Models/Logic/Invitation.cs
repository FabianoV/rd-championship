﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Invitation
    {
        public Guid Id { get; set; }
        public DateTime InvitationTime { get; set; }
        public Guid InvitingTeamId { get; set; }
        public string InvitingPerson { get; set; }
        public string InvitingPersonId { get; set; }
        public string InvitedPreson { get; set; }
        public string InvitedPresonId { get; set; }
    }
}
