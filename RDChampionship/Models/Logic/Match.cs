﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Match
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }

        public string TeamOnePlayerOneId { get; set; }
        public string TeamOnePlayerTwoId { get; set; }
        public string TeamTwoPlayerOneId { get; set; }
        public string TeamTwoPlayerTwoId { get; set; }

        public MatchStatus Status { get; set; }
        public int SetPointsTeamOne { get; set; }
        public int SetPointsTeamTwo { get; set; }
        public TeamChoice WinnerTeam { get; set; }
    }
}
