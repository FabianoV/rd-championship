﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class MatchScore
    {
        public double TeamOneSets { get; set; }
        public double TeamTwoSets { get; set; }
        public double TeamOnePoints { get; set; }
        public double TeamTwoPoints { get; set; }
    }
}
