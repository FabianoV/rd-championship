﻿using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class PlayersRankingRow
    {
        public int Place { get; set; }
        public AppUser Player { get; set; }
        public int Points { get; set; }
        public int PlayedMatches { get; set; }
    }
}
