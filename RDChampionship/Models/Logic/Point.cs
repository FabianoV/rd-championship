﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Point
    {
        public Guid Id { get; set; }
        public Guid MatchId { get; set; }
        public Guid SetId { get; set; }
        public DateTime Created { get; set; }
        public double Value { get; set; }
        public string OwnerTeamName { get; set; }
    }
}
