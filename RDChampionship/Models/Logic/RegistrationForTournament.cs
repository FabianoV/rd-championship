﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class RegistrationForTournament
    {
        public Guid Id { get; set; }
        public Guid TournamentId { get; set; }
        public string UserId { get; set; }
    }
}
