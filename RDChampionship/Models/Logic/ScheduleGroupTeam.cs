﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class GroupTeam
    {
        public Guid Id { get; set; }
        public Guid GroupId { get; set; }
        public Guid TournamentId { get; set; }
        public Guid TeamId { get; set; }
        public bool IsActive { get; set; }
    }
}
