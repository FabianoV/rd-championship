﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Set
    {
        public Guid Id { get; set; }
        public Guid MatchId { get; set; }
        public DateTime Created { get; set; }
        public bool IsEnded { get; set; }
        public int Number { get; set; }
        public double TeamOnePoints { get; set; }
        public double TeamTwoPoints { get; set; }
        public TeamChoice WinnerTeam { get; set; }
    }
}
