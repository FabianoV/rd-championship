﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Team
    {
        public Guid Id { get; set; }
        [Display(Name="Nazwa drużyny")]
        public string Name { get; set; }
        [Display(Name="Logo drużyny (Url)")]
        public string LogoUrl { get; set; }
        public string TeamOwnerId { get; set; }
        public string TeamMemberId { get; set; }
    }
}
