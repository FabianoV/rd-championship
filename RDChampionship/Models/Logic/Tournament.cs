﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class Tournament
    {
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime SignInEndDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public string LogoUrl { get; set; }
        public string Description { get; set; }
        [Required, Url]
        public string RulesPageUrl { get; set; }
        public TournamentPhase Phase { get; set; }
    }
}
