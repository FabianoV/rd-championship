﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models.Logic
{
    public class UserStatistics
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }


        // Global Statistics - tournaments and normal games
        /// <summary> Match count played in total. </summary>
        public int MatchesPlayed { get; set; }
        /// <summary> Total points scored in team where user was. </summary>
        public int PointsScoredInTeam { get; set; }

        // Tournament statistics
        /// <summary> Matches played in tournaments. </summary>
        public int TournamentMatchesPlayed { get; set; }
        /// <summary> Match count played in tournaments. </summary>
        public int TournamentPointsScored { get; set; }
    }
}
