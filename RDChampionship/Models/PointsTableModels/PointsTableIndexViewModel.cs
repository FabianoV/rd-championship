﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class PointsTableIndexViewModel
    {
        public Tournament Tournament { get; set; }
        public List<PlayersRankingRow> TableData { get; set; }
    }
}
