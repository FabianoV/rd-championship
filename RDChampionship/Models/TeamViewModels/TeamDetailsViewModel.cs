﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TeamDetailsViewModel
    {
        public Team Team { get; set; }
        public AppUser TeamOwner { get; set; }
        public AppUser TeamMember { get; set; }
    }
}
