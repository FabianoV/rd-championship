﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TeamListViewModel
    {
        public List<Team> Teams { get; set; }
        public List<AppUser> Users { get; set; }
    }
}
