﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TeamMemberViewModel
    {
        public AppUser TeamOwner { get; set; }
        public Team UserTeam { get; set; }
        public IEnumerable<Invitation> UserReceivedInvitations { get; set; }
    }
}
