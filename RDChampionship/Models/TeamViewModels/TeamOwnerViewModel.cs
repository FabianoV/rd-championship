﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TeamOwnerViewModel
    {
        public Team UserTeam { get; set; }
        public IEnumerable<Invitation> UserSendedInvitations { get; set; }
        public IEnumerable<Invitation> UserReceivedInvitations { get; set; }
    }
}
