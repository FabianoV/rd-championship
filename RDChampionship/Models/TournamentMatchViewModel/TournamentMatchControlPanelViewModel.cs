﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentMatchControlPanelViewModel
    {
        public TournamentMatch Match { get; set; }

        public AppUser TeamOnePlayerOne { get; set; }
        public AppUser TeamOnePlayerTwo { get; set; }
        public AppUser TeamTwoPlayerOne { get; set; }
        public AppUser TeamTwoPlayerTwo { get; set; }

        public MatchScore MatchScore { get; set; }

        public bool IsCurrentUserIsMatchPlayer { get; set; }



        //public List<Set> Sets { get; set; }
        //public List<Point> Points { get; set; }
        //public int TeamOneSets { get; set; }
        //public int TeamTwoSets { get; set; }
        //public int TeamOnePoints { get; set; }
        //public int TeamTwoPoints { get; set; }
    }
}
