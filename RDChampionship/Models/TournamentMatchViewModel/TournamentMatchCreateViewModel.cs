﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentMatchCreateViewModel
    {
        public TournamentMatch TournamentMatch { get; set; }
        public List<AppUser> TournamentParticipants { get; set; }
    }
}
