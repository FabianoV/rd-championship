﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentMatchIndexViewModel
    {
        public Tournament Tournament { get; set; }
        public List<TournamentMatch> Matches { get; set; }
        public List<AppUser> Users { get; set; }
    }
}
