﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentIndexViewModel
    {
        public List<Tournament> Tournaments { get; set; }
        public List<Group> Groups { get; set; }
    }
}
