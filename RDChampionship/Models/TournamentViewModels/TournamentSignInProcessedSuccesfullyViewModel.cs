﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentSignInProcessedSuccesfullyViewModel
    {
        public Tournament Tournament { get; set; }
    }
}
