﻿using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Models
{
    public class TournamentRegisteredUsersViewModel
    {
        public Guid TournamentId { get; set; }
        public List<RegistrationForTournament> Registrations { get; set; }
        public List<AppUser> Users { get; set; }
    }
}
