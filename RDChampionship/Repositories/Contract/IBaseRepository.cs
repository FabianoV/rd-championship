﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Contract
{
    public interface IBaseRepository<T>
    {
        List<T> GetAll();
        void Create(T item);
        void Update(T item);
        void Delete(T item);
    }
}
