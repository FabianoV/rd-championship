﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDChampionship.Models.Logic;

namespace RDChampionship.Repositories.Contract
{
    public interface IGroupRepository
    {
        List<Group> GetActiveGroups();
        List<Group> GetActiveGroupsForTournament(Guid tournamentId);

        Group Get(Guid id);
        Group Get(Guid tournamentId, string name);

        void SetAllGroupsUnactive(Guid tournamentId);
    }
}
