﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Contract
{
    public interface IInvitationRepository : IBaseRepository<Invitation>
    {
        List<Invitation> GetUserReceivedInvtiations(string userName);
        List<Invitation> GetUserSendedInvitations(string userName);

        Invitation Get(Guid id);

        void DeleteSendedInvitationsByUser(string userName);
    }
}
