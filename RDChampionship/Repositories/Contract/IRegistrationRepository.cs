﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDChampionship.Models.Logic;

namespace RDChampionship.Repositories.Contract
{
    public interface IRegistrationRepository
    {
        List<RegistrationForTournament> GetAll(Guid tournamentId);
        RegistrationForTournament Get(string userId, Guid tournamentId);

        void Create(RegistrationForTournament registration);

        void Delete(string userId, Guid tournamentId);
        bool IsUserRegisteredForTournament(string userId, Guid tournamentId);
    }
}
