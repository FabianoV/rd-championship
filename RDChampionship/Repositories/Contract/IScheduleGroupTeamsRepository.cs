﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDChampionship.Models.Logic;

namespace RDChampionship.Repositories.Contract
{
    public interface IGroupTeamsRepository
    {
        void SetAllGroupTeamsUnactive(string name);
        void CreateGroupMember(GroupTeam groupMember);
    }
}
