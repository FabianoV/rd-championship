﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDChampionship.Models.Logic;

namespace RDChampionship.Repositories.Contract
{
    public interface ISetRepository : IBaseRepository<Set>
    {
        List<Set> GetSetsForMatch(Guid matchId);
        Set GetCurrentSetForMatch(Guid matchId);
        Set GetByNumber(Guid matchId, int number);
    }
}
