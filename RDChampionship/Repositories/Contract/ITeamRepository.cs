﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Contract
{
    public interface ITeamRepository : IBaseRepository<Team>
    {
        List<Team> GetTeamsByParticipantUserName(string userName);

        Team Get(Guid id);
        Team Get(string name);
        Team GetByOwnerName(string ownerName);
        Team GetByMemberName(string memberName);

        bool IsUserOwnerOfTeam(string userName);
        bool IsUserMemeberOfTeam(string userName);

        //void DeleteTeamByName(string name);
        //void DeleteTeamByOwnerName(string ownerName);
    }
}
