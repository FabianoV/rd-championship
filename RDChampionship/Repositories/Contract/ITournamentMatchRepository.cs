﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RDChampionship.Models.Logic;
using RDChampionship.Services.Identity;

namespace RDChampionship.Repositories.Contract
{
    public interface ITournamentMatchRepository : IBaseRepository<TournamentMatch>
    {
        TournamentMatch Get(Guid id);
        List<TournamentMatch> GetAll(Guid tournamentId);
        List<TournamentMatch> GetAll(AppUser participant);
    }
}
