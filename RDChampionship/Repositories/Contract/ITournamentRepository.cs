﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Contract
{
    public interface ITournamentRepository : IBaseRepository<Tournament>
    {
        Tournament Get(Guid id);
        Tournament Get(string name);
    }
}
