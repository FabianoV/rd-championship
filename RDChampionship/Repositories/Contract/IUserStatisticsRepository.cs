﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Contract
{
    public interface IUserStatisticsRepository : IBaseRepository<UserStatistics>
    {
        UserStatistics Get(string userId);
    }
}
