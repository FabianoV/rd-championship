﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T 
        : class, new()
    {
        // Properties
        public ApplicationDbContext DbContext { get; set; }
        public abstract DbSet<T> Table { get; }

        // Constructor
        public BaseRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public List<T> GetAll()
        {
            return Table.ToList();
        }

        public void Create(T item)
        {
            DbContext.Add(item);
            DbContext.SaveChanges();
        }

        public void Update(T item)
        {
            DbContext.Update(item);
            DbContext.SaveChanges();
        }

        public void Delete(T item)
        {
            DbContext.Remove(item);
            DbContext.SaveChanges();
        }
    }
}
