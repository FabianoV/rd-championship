﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class GroupRepository : BaseRepository<Group>, IGroupRepository
    {
        // Properties
        public override DbSet<Group> Table => DbContext.Groups;

        // Constructor
        public GroupRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public Group Get(Guid id)
        {
            return DbContext.Groups.FirstOrDefault(g => g.Id == id);
        }

        public Group Get(Guid tournamentId, string name)
        {
            return DbContext.Groups.FirstOrDefault(g => g.TournamentId == tournamentId && g.Name == name);
        }

        public List<Group> GetActiveGroupsForTournament(Guid tournamentId)
        {
            return DbContext.Groups.Where(g => g.TournamentId == tournamentId && g.IsActive == true).ToList();
        }

        public void SetAllGroupsUnactive(Guid tournamentId)
        {
            var groups = DbContext.Groups.Where(g => g.TournamentId == tournamentId).ToList();
            foreach (var group in groups)
            {
                group.IsActive = false;
                DbContext.Update(group);
            }
            DbContext.SaveChanges();
        }

        public List<Group> GetActiveGroups()
        {
            return DbContext.Groups.Where(g => g.IsActive).ToList();
        }
    }
}
