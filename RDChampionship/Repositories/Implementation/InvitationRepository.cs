﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class InvitationRepository : BaseRepository<Invitation>, IInvitationRepository
    {
        // Properties
        public override DbSet<Invitation> Table => DbContext.Invitations;

        // Constructor
        public InvitationRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public Invitation Get(Guid id)
        {
            return DbContext.Invitations.FirstOrDefault(i => i.Id == id);
        }

        public List<Invitation> GetUserReceivedInvtiations(string userName)
        {
            return DbContext.Invitations.Where(i => i.InvitedPreson == userName).ToList();
        }

        public List<Invitation> GetUserSendedInvitations(string userName)
        {
            return DbContext.Invitations.Where(i => i.InvitingPerson == userName).ToList();
        }

        public void DeleteSendedInvitationsByUser(string userName)
        {
            var invitations = DbContext.Invitations.Where(i => i.InvitingPerson == userName).ToList();
            DbContext.RemoveRange(invitations);
            DbContext.SaveChanges();
        }
    }
}
