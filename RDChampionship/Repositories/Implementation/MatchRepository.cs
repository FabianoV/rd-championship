﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RDChampionship.Repositories.Implementation
{
    public class MatchRepository : BaseRepository<Match>, IMatchRepository
    {
        // Properties
        public override DbSet<Match> Table => DbContext.Matches;

        // Constructor
        public MatchRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public Match Get(Guid id)
        {
            return DbContext.Matches.FirstOrDefault(m => m.Id == id);
        }
    }
}
