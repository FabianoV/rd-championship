﻿using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class RegistrationRepository : IRegistrationRepository
    {
        // Properties
        public ApplicationDbContext DbContext { get; set; }

        // Constructor
        public RegistrationRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public List<RegistrationForTournament> GetAll(Guid tournamentId)
        {
            return DbContext.RegistrationForTournaments.Where(rft => rft.TournamentId == tournamentId).ToList();
        }

        public RegistrationForTournament Get(string userId, Guid tournamentId)
        {
            return DbContext.RegistrationForTournaments.FirstOrDefault(rft => rft.UserId == userId && rft.TournamentId == tournamentId);
        }

        public bool IsUserRegisteredForTournament(string userId, Guid tournamentId)
        {
            return DbContext.RegistrationForTournaments.Any(rft => rft.UserId == userId && rft.TournamentId == tournamentId);
        }

        public void Create(RegistrationForTournament registration)
        {
            DbContext.Add(registration);
            DbContext.SaveChanges();
        }

        public void Delete(string userId, Guid tournamentId)
        {
            var registration = DbContext.RegistrationForTournaments.FirstOrDefault(r => r.UserId == userId && r.TournamentId == tournamentId);
            if (registration != null)
            {
                DbContext.Remove(registration);
                DbContext.SaveChanges();
            }
        }
    }
}
