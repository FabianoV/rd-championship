﻿using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class ScheduleGroupTeamsRepository : IGroupTeamsRepository
    {
        // Properties
        public ApplicationDbContext DbContext { get; set; }

        // Constructor
        public ScheduleGroupTeamsRepository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
        }

        // Methods
        public void SetAllGroupTeamsUnactive(string name)
        {
            var scheduleGroupTeams = DbContext.ScheduleGroupTeams.ToList();
            scheduleGroupTeams.ForEach(t =>
            {
                t.IsActive = false;
                DbContext.Update(t);
            });
            DbContext.SaveChanges();
        }

        public void CreateGroupMember(GroupTeam groupMember)
        {
            DbContext.Add(groupMember);
            DbContext.SaveChanges();
        }
    }
}
