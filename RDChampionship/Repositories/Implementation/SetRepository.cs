﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class SetRepository : BaseRepository<Set>, ISetRepository
    {
        // Properties
        public override DbSet<Set> Table => DbContext.Sets;

        // Constructor
        public SetRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public Set Get(Guid id)
        {
            return DbContext.Sets.FirstOrDefault(s => s.Id == id);
        }

        public Set GetCurrentSetForMatch(Guid matchId)
        {
            return DbContext.Sets.Where(m => m.MatchId == matchId).OrderByDescending(m => m.Number).FirstOrDefault();
        }

        public List<Set> GetSetsForMatch(Guid matchId)
        {
            return DbContext.Sets.Where(m => m.MatchId == matchId).ToList();
        }

        public Set GetByNumber(Guid matchId, int number)
        {
            return DbContext.Sets.FirstOrDefault(m => m.MatchId == matchId && m.Number == number);
        }
    }
}
