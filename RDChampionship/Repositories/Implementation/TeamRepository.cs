﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class TeamRepository : BaseRepository<Team>, ITeamRepository
    {
        // Properties
        public override DbSet<Team> Table => DbContext.Teams;

        // Constructor
        public TeamRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public Team Get(Guid id)
        {
            return DbContext.Teams.FirstOrDefault(t => t.Id == id);
        }
        public Team Get(string name)
        {
            return DbContext.Teams.FirstOrDefault(t => t.Name == name);
        }

        public Team GetByOwnerName(string ownerName)
        {
            var user = DbContext.Users.First(u => u.UserName == ownerName);
            return DbContext.Teams.FirstOrDefault(t=>t.TeamOwnerId == user.Id);
        }

        public Team GetByMemberName(string memberName)
        {
            var user = DbContext.Users.First(u => u.UserName == memberName);
            return DbContext.Teams.FirstOrDefault(t => t.TeamMemberId == user.Id);
        }

        public List<Team> GetTeamsByParticipantUserName(string userName)
        {
            var user = DbContext.Users.First(u => u.UserName == userName);
            return DbContext.Teams.Where(t => t.TeamMemberId == user.Id || t.TeamOwnerId == user.Id).ToList();
        }

        public bool IsUserOwnerOfTeam(string userName)
        {
            var user = DbContext.Users.First(u => u.UserName == userName);
            return DbContext.Teams.Any(t => t.TeamOwnerId == user.Id);
        }

        public bool IsUserMemeberOfTeam(string userName)
        {
            var user = DbContext.Users.First(u => u.UserName == userName);
            return DbContext.Teams.Any(t => t.TeamMemberId == user.Id);
        }
    }
}
