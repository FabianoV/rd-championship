﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class TournamentMatchRepository : BaseRepository<TournamentMatch>, ITournamentMatchRepository
    {
        // Properties
        public override DbSet<TournamentMatch> Table => DbContext.TournamentMatches;

        // Constructor
        public TournamentMatchRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public TournamentMatch Get(Guid id)
        {
            return DbContext.TournamentMatches.FirstOrDefault(m => m.Id == id);
        }

        public List<TournamentMatch> GetAll(Guid tournamentId)
        {
            return DbContext.TournamentMatches.Where(m => m.TournamentId == tournamentId).ToList();
        }

        public List<TournamentMatch> GetAll(AppUser participant)
        {
            return DbContext.TournamentMatches.Where(m => m.TeamOnePlayerOneId == participant.Id ||
                                                          m.TeamOnePlayerTwoId == participant.Id ||
                                                          m.TeamTwoPlayerOneId == participant.Id ||
                                                          m.TeamTwoPlayerTwoId == participant.Id).ToList();
        }
    }
}
