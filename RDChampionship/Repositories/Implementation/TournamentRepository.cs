﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class TournamentRepository : BaseRepository<Tournament>, ITournamentRepository
    {
        // Properties
        public override DbSet<Tournament> Table => DbContext.Tournaments;

        // Constructor
        public TournamentRepository(ApplicationDbContext dbContext) 
            : base(dbContext)
        {
        }

        // Methods
        public Tournament Get(Guid id)
        {
            return DbContext.Tournaments.FirstOrDefault(t => t.Id == id);
        }

        public Tournament Get(string name)
        {
            return DbContext.Tournaments.FirstOrDefault(t => t.Name == name);
        }
    }
}
