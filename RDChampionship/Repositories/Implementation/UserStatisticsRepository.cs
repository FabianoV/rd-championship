﻿using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Repositories.Implementation
{
    public class UserStatisticsRepository : BaseRepository<UserStatistics>, IUserStatisticsRepository
    {
        // Properties
        public override DbSet<UserStatistics> Table => DbContext.UserStatistics;

        // Constructor
        public UserStatisticsRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
        }

        // Methods
        public UserStatistics Get(string userId)
        {
            return DbContext.UserStatistics.FirstOrDefault(us => us.UserId.ToString() == userId);
        }
    }
}
