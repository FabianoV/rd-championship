﻿using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IAppRoleService
    {
        List<AppRole> AppRoles { get; }

        void DefineRoles();
        Task EnsureAppRolesExists();
    }
}
