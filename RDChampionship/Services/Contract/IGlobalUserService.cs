﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IGlobalUserService
    {
        Task EnsureGlobalUserExists();
        Task EnsureGlobalUserHasGlobalRoles();
    }
}
