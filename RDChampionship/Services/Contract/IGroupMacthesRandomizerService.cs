﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IGroupMacthesRandomizerService
    {
        List<TournamentMatch> CreateMatchesForGroup(Guid tournamentId, Guid groupId, int rounds);
    }
}
