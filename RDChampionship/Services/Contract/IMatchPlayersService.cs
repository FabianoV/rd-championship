﻿using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IMatchPlayersService
    {
        Task<List<AppUser>> GetPlayers(Guid matchId);
        Task<List<AppUser>> GetTeamOnePlayers(Guid matchId);
        Task<List<AppUser>> GetTeamTwoPlayers(Guid matchId);
        Task<List<AppUser>> GetWinnerTeamPlayers(Guid matchId);
        Task<List<AppUser>> GetLostTeamPlayers(Guid matchId);
    }
}
