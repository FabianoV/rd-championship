﻿using RDChampionship.Repositories.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IRepositoriesService
    {
        IGroupRepository GroupRepository { get; }
        IInvitationRepository InvitationRepository { get; }
        IMatchRepository MatchRepository { get; }
        IPointsRepository PointsRepository { get; }
        IRegistrationRepository RegistrationRepository { get; }
        ISetRepository SetRepository { get; }
        ITeamRepository TeamRepository { get; }
        ITournamentMatchRepository TournamentMatchRepository { get; }
        ITournamentRepository TournamentRepository { get; }
        IUserStatisticsRepository UserStatisticsRepository { get; }

    }
}
