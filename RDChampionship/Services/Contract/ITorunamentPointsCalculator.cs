﻿using RDChampionship.Models;
using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IPlayersRankingService
    {
        List<PlayersRankingRow> CalculateRanking(Guid tournamentId, RankingSortKey sortKey);
    }
}
