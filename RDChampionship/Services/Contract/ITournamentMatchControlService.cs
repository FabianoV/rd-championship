﻿using RDChampionship.Models.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface ITournamentMatchControlService
    {
        MatchScore GetCurrentMatchScore(Guid matchId);
        void StartMatch(Guid matchId);
        void EndMatch(Guid matchId);
        void AddPoint(Guid matchId, string team);
        void SubstractPoint(Guid matchId, string team);
        void NextSet(Guid matchId);
        void PreviousSet(Guid matchId);
    }
}
