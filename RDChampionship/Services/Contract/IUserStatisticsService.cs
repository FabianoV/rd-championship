﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Contract
{
    public interface IUserStatisticsService
    {
        void SumStatisticsForMatch(string userId, Guid matchId);
        void SumStatisticsForTournamentMatch(string userId, Guid tournamentMatchId);
    }
}
