﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Identity
{
    public class AppRoleManager : RoleManager<AppRole>
    {
        // Constructor
        public AppRoleManager(IRoleStore<AppRole> store, IEnumerable<IRoleValidator<AppRole>> roleValidators, 
            ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, ILogger<RoleManager<AppRole>> logger) 
            : base(store, roleValidators, keyNormalizer, errors, logger)
        {

        }

        // Methods
        
    }
}
