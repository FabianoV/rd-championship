﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Identity
{
    public class AppRoleStore : RoleStore<AppRole, ApplicationDbContext, string, IdentityUserRole<string>, IdentityRoleClaim<string>>
    {
        public AppRoleStore(ApplicationDbContext context) 
            : base(context, describer: null)
        {
        }
    }
}
