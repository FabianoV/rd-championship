﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Identity
{
    public class AppUser : IdentityUser<string>
    {
        public string AvatarUrl { get; set; }
    }
}
