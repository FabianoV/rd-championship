﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Identity
{
    public class AppUserStore : UserStore<AppUser, AppRole, ApplicationDbContext, string, IdentityUserClaim<string>, 
        IdentityUserRole<string>, IdentityUserLogin<string>, IdentityUserToken<string>, IdentityRoleClaim<string>>
    {
        // Constructor
        public AppUserStore(ApplicationDbContext context, IdentityErrorDescriber describer = null) 
            : base(context, describer)
        {
        }
    }
}
