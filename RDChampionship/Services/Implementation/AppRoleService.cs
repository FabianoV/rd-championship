﻿using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    /// <summary>
    /// Service responsible for create and contains application roles.
    /// </summary>
    public class AppRoleService : IAppRoleService
    {
        // Properties
        public AppRoleManager RoleManager { get; set; }
        public List<AppRole> AppRoles { get; set; }
        public AppRoleStore RoleStore { get; set; }

        // Roles Names Constants
        public const string Administrator = "Administrator";
        public const string Moderator = "Moderator";
        public const string Participant = "Participant";
        public const string FreeMatchPlayer = "FreeMatchPlayer";

        // Roles
        public AppRole AdministratorRole { get; set; }  // Global privigiles
        public AppRole ModeratorRole { get; set; }      // Can edit some Matches and Tournaments
        public AppRole ParticipantRole { get; set; }    // Can sign in and exist in tournament
        public AppRole FreeMatchPlayerRole { get; set; }    // Can use free match functionalities

        // Constructor
        public AppRoleService(AppRoleManager roleManager)
        {
            RoleManager = roleManager;
            DefineRoles();
        }

        // Methods
        public void DefineRoles()
        {
            AdministratorRole = new AppRole()
            {
                Id = "48254cf1-2750-4fd4-9a8e-15f491ba8262",
                Name = Administrator,
            };

            ModeratorRole = new AppRole()
            {
                Id = "824e7c64-4825-4c3d-b6da-55c0b3f462ce",
                Name = Moderator,
            };

            ParticipantRole = new AppRole()
            {
                Id = "ad31d0fd-c4aa-485c-ab4a-c778ec897b63",
                Name = Participant,
            };

            FreeMatchPlayerRole = new AppRole()
            {
                Id = "75d6491b-212c-49cd-a6f9-02df52a7e906",
                Name = FreeMatchPlayer,
            };

            AppRoles = new List<AppRole>()
            {
                AdministratorRole,
                ModeratorRole,
                ParticipantRole,
                FreeMatchPlayerRole,
            };
        }

        public async Task EnsureAppRolesExists()
        {
            foreach (var role in AppRoles)
            {
                bool roleExists = await RoleManager.RoleExistsAsync(role.Name);
                if (!roleExists)
                {
                    var result = await RoleManager.CreateAsync(role);
                }
            }
        }
    }
}
