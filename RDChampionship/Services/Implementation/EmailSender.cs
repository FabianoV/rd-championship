﻿using Microsoft.AspNetCore.Identity.UI.Services;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    public class EmailSender : IEmailSender
    {
        // Properties
        public virtual string SendGridApiKeyName { get; set; } = "RdChampionshipSendGridKey";
        public virtual string SendGridApiKey { get; set; } = "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";

        // Methods
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SendGridClient(SendGridApiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress("test@example.com", "RD Championship Administracja"),
                Subject = subject,
                PlainTextContent = "",
                HtmlContent = htmlMessage
            };
            msg.AddTo(new EmailAddress(email));

            // Disable click tracking.
            // See https://sendgrid.com/docs/User_Guide/Settings/tracking.html
            msg.SetClickTracking(false, false);

            return client.SendEmailAsync(msg);
        }
    }
}
