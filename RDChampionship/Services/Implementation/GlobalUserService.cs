﻿using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    /// <summary>
    /// Service responsible for create and manage Global Administrator user account.
    /// </summary>
    public class GlobalUserService : IGlobalUserService
    {
        // Consts
        public const string GmailAccountEmail = "rdchampionshipglobal@gmail.com";
        public const string GmailAccountPassword = "1qaz@WSXC";

        // Services
        public AppUserManager UserManager { get; set; }
        public IAppRoleService AppRoleService { get; set; }
        public AppRoleManager RoleManager { get; set; }
        public AppSignInManager SignInManager { get; set; }

        // Properties
        public virtual string GlobalAdminPassword { get; set; } = "1qaz@WSXC";
        public virtual AppUser GlobalAdministrator { get; set; } = new AppUser()
        {
            Id = "6d77fdff-4de7-4595-a07f-dc553e04f8c9",
            Email = GmailAccountEmail,
            UserName = GmailAccountEmail,
            EmailConfirmed = true,
        };

        // Constructor
        public GlobalUserService(AppUserManager userManager, IAppRoleService appRoleService, AppRoleManager appRoleManager, AppSignInManager signInManager)
        {
            UserManager = userManager;
            RoleManager = appRoleManager;
            AppRoleService = appRoleService;
            SignInManager = signInManager;
        }

        // Methods
        public async Task EnsureGlobalUserExists()
        {
            var user = await UserManager.FindByIdAsync(GlobalAdministrator.Id);
            if (user == null)
            {
                var result = await UserManager.CreateAsync(GlobalAdministrator, GlobalAdminPassword);

                if (!result.Succeeded)
                {
                    throw new Exception("Cannot create Global Administrator account when it not exists!");
                }
            }
        }

        public async Task EnsureGlobalUserHasGlobalRoles()
        {
            foreach (var role in AppRoleService.AppRoles)
            {
                var user = UserManager.Users.FirstOrDefault(u => u.Id == GlobalAdministrator.Id);
                bool isIncurrentRole = await UserManager.IsInRoleAsync(user, role.Name);
                if (!isIncurrentRole)
                {
                    var result = await UserManager.AddToRoleAsync(user, role.Name);

                    if (!result.Succeeded)
                    {
                        throw new Exception($"Cannot assign role [{role.Name}] to Global administrator!");
                    }
                }
            }
        }
    }
}
