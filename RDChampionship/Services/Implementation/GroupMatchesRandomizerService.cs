﻿using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RDChampionship.Services.Implementation
{
    //public class GroupMatchesRandomizerService : IGroupMacthesRandomizerService
    //{
    //    // Properties
    //    public ITournamentRepository TournamentRepository { get; set; }
    //    public IGroupRepository GroupRepository { get; set; }
    //    public IGroupTeamsRepository GroupTeamsRepository { get; set; }
    //    public ITournamentMatchRepository MatchRepository { get; set; }

    //    // Constructor
    //    public GroupMatchesRandomizerService(IGroupTeamsRepository groupTeamsRepository, ITournamentRepository tournamentRepository,
    //        ITournamentMatchRepository matchRepository, IGroupRepository groupRepository)
    //    {
    //        TournamentRepository = tournamentRepository;
    //        GroupRepository = groupRepository;
    //        GroupTeamsRepository = groupTeamsRepository;
    //        MatchRepository = matchRepository;
    //    }

    //    // Methods
    //    public List<TournamentMatch> CreateMatchesForGroup(Guid tournamentId, Guid groupId, int rounds)
    //    {
    //        var creationTime = DateTime.Now;
    //        var matches = new List<TournamentMatch>();

    //        var tournament = TournamentRepository.GetTournamentById(tournamentId);
    //        var group = GroupRepository.GetGroup(groupId);
    //        // var groupTeams = GroupTeamsRepository.GetActiveGroupTeamsByGroupName(tournamentId, group.Name);

    //        // MatchRepository.SetMatchesUnactiveForGroup(tournamentId, groupId);

    //        if (groupTeams.Count < 2)
    //        {
    //            return matches; // no matches for one or zero teams...
    //        }

    //        foreach (var teamOne in groupTeams)
    //        {
    //            foreach (var teamTwo in groupTeams)
    //            {
    //                if (!teamOne.Equals(teamTwo) //&&
    //                    //!matches.Any(m => m.TeamOneId == teamOne.Id && m.TeamTwoId == teamTwo.Id) &&
    //                    //!matches.Any(m => m.TeamOneId == teamTwo.Id && m.TeamTwoId == teamOne.Id)
    //                    )
    //                {
    //                    for (int round = 0; round < rounds; round++)
    //                    {
    //                        var match = new TournamentMatch
    //                        {
    //                            Name = $"{tournament.Name} match",
    //                            TournamentId = tournament.Id,
    //                            Created = creationTime,
    //                            StartTime = DateTime.MinValue,
    //                            IsActive = true,
    //                        };
    //                        matches.Add(match);
    //                        MatchRepository.CreateMatch(match);
    //                    }
    //                }
    //            }
    //        }

    //        return matches;
    //    }
    //}
}
