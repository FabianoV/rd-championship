﻿using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    public class MatchPlayersService : IMatchPlayersService
    {
        // Properties
        public AppUserManager UserManager { get; set; }
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }

        // Constructor
        public MatchPlayersService(AppUserManager userManager, ITournamentMatchRepository tournamentMatchRepository)
        {
            UserManager = userManager;
            TournamentMatchRepository = tournamentMatchRepository;
        }

        // Methods
        public async Task<List<AppUser>> GetLostTeamPlayers(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);

            switch (match.WinnerTeam)
            {
                case TeamChoice.None: return await GetPlayers(match);
                case TeamChoice.TeamOne: return await GetTeamOnePlayers(match);
                case TeamChoice.TeamTwo: return await GetTeamOnePlayers(match);
                case TeamChoice.TeamOneAndTeamTwo: return new List<AppUser>(); // return empty list if two teams won
                default: throw new NotImplementedException();
            }
        }

        public async Task<List<AppUser>> GetWinnerTeamPlayers(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);

            switch (match.WinnerTeam)
            {
                case TeamChoice.None: return new List<AppUser>(); // return empty list if anyone not won
                case TeamChoice.TeamOne: return await GetTeamOnePlayers(match);
                case TeamChoice.TeamTwo: return await GetTeamOnePlayers(match);
                case TeamChoice.TeamOneAndTeamTwo: return await GetPlayers(match);
                default: throw new NotImplementedException();
            }
        }

        public async Task<List<AppUser>> GetPlayers(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            return await GetPlayers(match);
        }

        public async Task<List<AppUser>> GetTeamOnePlayers(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            return await GetTeamOnePlayers(match);
        }

        public async Task<List<AppUser>> GetTeamTwoPlayers(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            return await GetTeamTwoPlayers(match);
        }

        private async Task<List<AppUser>> GetTeamOnePlayers(Match match)
        {
            if (match.Status != MatchStatus.Ended)
            {
                throw new Exception("Cannot find players from lost team before match was ended.");
            }

            var players = new List<AppUser>();

            var player1 = await UserManager.FindByIdAsync(match.TeamOnePlayerOneId);
            var player2 = await UserManager.FindByIdAsync(match.TeamOnePlayerTwoId);

            players.Add(player1);
            players.Add(player2);

            return players;
        }

        private async Task<List<AppUser>> GetTeamTwoPlayers(Match match)
        {
            if (match.Status != MatchStatus.Ended)
            {
                throw new Exception("Cannot find players from lost team before match was ended.");
            }

            var players = new List<AppUser>();

            var player1 = await UserManager.FindByIdAsync(match.TeamTwoPlayerOneId);
            var player2 = await UserManager.FindByIdAsync(match.TeamTwoPlayerTwoId);

            players.Add(player1);
            players.Add(player2);

            return players;
        }

        private async Task<List<AppUser>> GetPlayers(Match match)
        {
            var players = new List<AppUser>();
            players.AddRange(await GetTeamOnePlayers(match));
            players.AddRange(await GetTeamTwoPlayers(match));
            return players;
        }
    }
}
