﻿using RDChampionship.Data;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    //public class RegisteredTeamsToGroupsService : IRegisteredTeamsToGroupsService
    //{
    //    // Properties
    //    public ITeamRepository TeamRepository { get; set; }
    //    public ITournamentRepository TournamentRepository { get; set; }
    //    public ITournamentSettingsRepository TournamentSettingsRepository { get; set; }
    //    public IGroupRepository GroupRepository { get; set; }
    //    public IGroupTeamsRepository ScheduleGroupTeamsRepository { get; set; }

    //    public Random Random { get; set; }

    //    // Consructor
    //    public RegisteredTeamsToGroupsService(ApplicationDbContext dbContext, ITeamRepository teamRepository, IGroupRepository groupRepository,
    //        ITournamentSettingsRepository tournamentSettingsRepository, ITournamentRepository tournamentRepository, IGroupTeamsRepository scheduleGroupTeamsRepository)
    //    {
    //        TeamRepository = teamRepository;
    //        TournamentSettingsRepository = tournamentSettingsRepository;
    //        TournamentRepository = tournamentRepository;
    //        GroupRepository = groupRepository;
    //        ScheduleGroupTeamsRepository = scheduleGroupTeamsRepository;
    //        Random = new Random();
    //    }

    //    // Methods
    //    public TournamentSettings RandomMatches(Guid tournamentId, int groupCount)
    //    {
    //        var tournament = TournamentRepository.GetTournamentById(tournamentId);
    //        var settings = CreateNewTournamentSettings(tournament);
    //        var groups = CreateScheduleGroupsForTournamentSettings(settings, tournament, groupCount);
    //        var registeredTeams = CreateScheduleTeamsForTournamentSettings(tournament, groups);
    //        return settings;
    //    }

    //    public List<Team> CreateScheduleTeamsForTournamentSettings(Tournament tournament, List<Group> groups)
    //    {
    //        var teamsRegistered = TeamRepository.GetRegisteredTeams(tournament.Id);
    //        teamsRegistered = teamsRegistered.Where(t => t.TeamMemberId != null).ToList(); // Team must be full.

    //        ScheduleGroupTeamsRepository.SetAllGroupTeamsUnactive(tournament.Name);
    //        ShuffleTeams(teamsRegistered);
    //        AssignTeamsToGroups(groups, teamsRegistered, tournament);

    //        return teamsRegistered;
    //    }

    //    public TournamentSettings CreateNewTournamentSettings(Tournament tournament)
    //    {
    //        TournamentSettingsRepository.SetAllSettingsUnactive(tournament.Id);
    //        var tournamentSettings = new TournamentSettings()
    //        {
    //            TournamentId = tournament.Id,
    //            TournamentName = tournament.Name,
    //            Created = DateTime.Now,
    //            IsActive = true,
    //        };
    //        TournamentSettingsRepository.CreateTournamentSettings(tournamentSettings);
    //        return tournamentSettings;
    //    }

    //    public List<Group> CreateScheduleGroupsForTournamentSettings(TournamentSettings settings, Tournament tournament, int groupCount)
    //    {
    //        GroupRepository.SetAllGroupsUnactive(tournament.Id);

    //        var groups = new List<Group>();
    //        for (int groupNumber = 1; groupNumber <= groupCount; groupNumber++)
    //        {
    //            var group = new Group()
    //            {
    //                Name = $"Grupa {groupNumber}",
    //                TournamentId = tournament.Id,
    //                TournamentName = tournament.Name,
    //                IsActive = true,
    //            };
    //            GroupRepository.CreateGroup(group);
    //            groups.Add(group);
    //        }

    //        return groups;
    //    }

    //    public List<Team> ShuffleTeams(List<Team> list)
    //    {
    //        int n = list.Count;
    //        while (n > 1)
    //        {
    //            n--;
    //            int k = Random.Next(n + 1);
    //            Team value = list[k];
    //            list[k] = list[n];
    //            list[n] = value;
    //        }
    //        return list;
    //    }

    //    public void AssignTeamsToGroups(List<Group> groups, List<Team> teamsRegistered, Tournament tournament)
    //    {
    //        int currentGroupIndex = 0;
    //        foreach (var team in teamsRegistered)
    //        {
    //            var currentGroup = groups[currentGroupIndex];
    //            var groupMember = new GroupTeam()
    //            {
    //                TeamId = team.Id,
    //                TeamName = team.Name,
    //                GroupId = currentGroup.Id,
    //                GroupName = currentGroup.Name,
    //                TournamentId = tournament.Id,
    //                TournamentName = tournament.Name,
    //                IsActive = true,
    //            };
    //            ScheduleGroupTeamsRepository.CreateGroupMember(groupMember);
    //            currentGroupIndex = (currentGroupIndex + 1) % groups.Count;
    //        }
    //    }
    //}
}
