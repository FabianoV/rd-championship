﻿using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    /// <summary>
    /// Facade for all repositories instances for simplify.
    /// </summary>
    public class RepositoriesService : IRepositoriesService
    {
        // Repositories
        public IGroupRepository GroupRepository { get; set; }
        public IInvitationRepository InvitationRepository { get; set; }
        public IMatchRepository MatchRepository { get; set; }
        public IPointsRepository PointsRepository { get; set; }
        public IRegistrationRepository RegistrationRepository { get; set; }
        public ISetRepository SetRepository { get; set; }
        public ITeamRepository TeamRepository { get; set; }
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }
        public ITournamentRepository TournamentRepository { get; set; }
        public IUserStatisticsRepository UserStatisticsRepository { get; set; }

        // Constructor
        public RepositoriesService(
            IGroupRepository groupRepository,
            IInvitationRepository invitationRepository,
            IMatchRepository matchRepository,
            IPointsRepository pointsRepository,
            IRegistrationRepository registrationRepository,
            ISetRepository setRepository,
            ITeamRepository teamRepository,
            ITournamentMatchRepository tournamentMatchRepository,
            ITournamentRepository tournamentRepository,
            IUserStatisticsRepository userStatisticsRepository)
        {
            GroupRepository = groupRepository;
            InvitationRepository = invitationRepository;
            MatchRepository = matchRepository;
            PointsRepository = pointsRepository;
            RegistrationRepository = registrationRepository;
            SetRepository = setRepository;
            TeamRepository = teamRepository;
            TournamentMatchRepository = tournamentMatchRepository;
            TournamentRepository = TournamentRepository;
            UserStatisticsRepository = userStatisticsRepository;
        }
    }
}
