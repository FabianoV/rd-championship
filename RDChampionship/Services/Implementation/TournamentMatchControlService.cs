﻿using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    public class TournamentMatchControlService : ITournamentMatchControlService
    {
        // Properties
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }
        public ISetRepository SetRepository { get; set; }

        // Constructor
        public TournamentMatchControlService(ITournamentMatchRepository tournamentMatchRepository, ISetRepository setRepository)
        {
            TournamentMatchRepository = tournamentMatchRepository;
            SetRepository = setRepository;
        }

        // Methods
        public MatchScore GetCurrentMatchScore(Guid matchId)
        {
            var sets = SetRepository.GetSetsForMatch(matchId);
            var score = new MatchScore()
            {
                TeamOneSets = sets.Count(s => s.TeamOnePoints > s.TeamTwoPoints && s.IsEnded),
                TeamTwoSets = sets.Count(s => s.TeamTwoPoints > s.TeamOnePoints && s.IsEnded),
            };

            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);
            if (currentSet != null)
            {
                score.TeamOnePoints = currentSet.TeamOnePoints;
                score.TeamTwoPoints = currentSet.TeamTwoPoints;
            }

            return score;
        }

        public void StartMatch(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            match.StartTime = DateTime.Now;
            match.Status = MatchStatus.During;
            TournamentMatchRepository.Update(match);

            var set = new Set
            {
                Created = DateTime.Now,
                MatchId = match.Id,
                Number = 0,
                IsEnded = false,
                TeamOnePoints = 0,
                TeamTwoPoints = 0,
            };
            SetRepository.Create(set);
        }

        public void EndMatch(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);

            currentSet.IsEnded = true;
            if (currentSet.TeamOnePoints > currentSet.TeamTwoPoints)
            {
                currentSet.WinnerTeam = TeamChoice.TeamOne;
            }
            else if (currentSet.TeamOnePoints < currentSet.TeamTwoPoints)
            {
                currentSet.WinnerTeam = TeamChoice.TeamTwo;
            }
            else
            {
                currentSet.WinnerTeam = TeamChoice.TeamOneAndTeamTwo;
            }
            match.Status = MatchStatus.Ended;
            SetRepository.Update(currentSet);
            TournamentMatchRepository.Update(match);
        }

        public void AddPoint(Guid matchId, string team)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var set = SetRepository.GetCurrentSetForMatch(matchId);

            if (team == "one")
            {
                set.TeamOnePoints += 1;
            }

            if (team == "two")
            {
                set.TeamTwoPoints += 1;
            }

            SetRepository.Update(set);
        }

        public void SubstractPoint(Guid matchId, string team)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var set = SetRepository.GetCurrentSetForMatch(matchId);

            if (team == "one")
            {
                set.TeamOnePoints -= 1;
            }

            if (team == "two")
            {
                set.TeamTwoPoints -= 1;
            }

            SetRepository.Update(set);
        }

        public void NextSet(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);

            currentSet.IsEnded = true;
            if (currentSet.TeamOnePoints > currentSet.TeamTwoPoints)
            {
                currentSet.WinnerTeam = TeamChoice.TeamOne;
            }
            else if (currentSet.TeamOnePoints < currentSet.TeamTwoPoints)
            {
                currentSet.WinnerTeam = TeamChoice.TeamTwo;
            }
            else
            {
                currentSet.WinnerTeam = TeamChoice.TeamOneAndTeamTwo;
            }
            SetRepository.Update(currentSet);

            var set = new Set
            {
                Created = DateTime.Now,
                IsEnded = false,
                MatchId = matchId,
                Number = currentSet.Number + 1,
            };
            SetRepository.Create(set);
        }

        public void PreviousSet(Guid matchId)
        {
            var match = TournamentMatchRepository.Get(matchId);
            var currentSet = SetRepository.GetCurrentSetForMatch(matchId);

            if (currentSet.Number > 0)
            {
                var previousSet = SetRepository.GetByNumber(matchId, currentSet.Number - 1);
                SetRepository.Delete(currentSet);
                previousSet.IsEnded = false;
                previousSet.WinnerTeam = TeamChoice.None;
                SetRepository.Update(previousSet);
            }
        }
    }
}
