﻿using RDChampionship.Models;
using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    public class PlayerRankingService : IPlayersRankingService
    {
        // Properties
        public AppUserManager UserManager { get; set; }
        public IRegistrationRepository Registrations { get; set; }
        public ITournamentMatchRepository Matches { get; set; }

        // Constructor
        public PlayerRankingService(AppUserManager userManager, IRegistrationRepository registrations, ITournamentMatchRepository matches)
        {
            UserManager = userManager;
            Registrations = registrations;
            Matches = matches;
        }

        // Methods
        public List<PlayersRankingRow> CalculateRanking(Guid tournamentId, RankingSortKey sortKey)
        {
            var users = UserManager.Users.ToList();
            var participants = users.Where(u => Registrations.IsUserRegisteredForTournament(u.Id, tournamentId));
            var result = new List<PlayersRankingRow>();

            foreach (var user in participants)
            {
                var rankingRow = new PlayersRankingRow();
                rankingRow.Player = user;
                rankingRow.PlayedMatches = 0;
                result.Add(rankingRow);
            }

            result = CountMatchesAndPoints(result);
            result = SortData(result, sortKey);
            result = CaluculatePlaces(result);
            return result;
        }

        public List<PlayersRankingRow> CountMatchesAndPoints(List<PlayersRankingRow> data)
        {
            foreach (PlayersRankingRow rankingRow in data)
            {
                var currentPlayer = rankingRow.Player;
                var playerMatches = Matches.GetAll(currentPlayer).Where(m => m.Status == MatchStatus.Ended).ToList();
                rankingRow.PlayedMatches = playerMatches.Count;
                rankingRow.Points = 0;

                foreach (TournamentMatch match in playerMatches)
                {
                    var isCurrentPlayerInTeamOne = currentPlayer.Id == match.TeamOnePlayerOneId || currentPlayer.Id == match.TeamOnePlayerTwoId;
                    var isCurrentPlayerInTeamTwo = currentPlayer.Id == match.TeamTwoPlayerOneId || currentPlayer.Id == match.TeamTwoPlayerTwoId;

                    if (isCurrentPlayerInTeamOne)
                    {
                        rankingRow.Points += match.SetPointsTeamOne;
                    }

                    if (isCurrentPlayerInTeamTwo)
                    {
                        rankingRow.Points += match.SetPointsTeamTwo;
                    }
                }
            }

            return data;
        }

        private List<PlayersRankingRow> CaluculatePlaces(List<PlayersRankingRow> data)
        {
            foreach (var place in Enumerable.Range(1, data.Count))
            {
                data[place - 1].Place = place;
            }
            return data;
        }

        private List<PlayersRankingRow> SortData(List<PlayersRankingRow> data, RankingSortKey sortKey)
        {
            Func<PlayersRankingRow, PlayersRankingRow, int> comparationFunc = null;

            switch (sortKey)
            {
                case RankingSortKey.PositionAscending:
                    comparationFunc = (row1, row2) => { return row1.Place < row2.Place ? 1 : -1; };
                    break;
                case RankingSortKey.PositionDescending:
                    comparationFunc = (row1, row2) => { return row1.Place > row2.Place ? 1 : -1; };
                    break;
                case RankingSortKey.PointsDescending:
                    comparationFunc = (row1, row2) => { return row1.Points < row2.Points ? 1 : -1; };
                    break;
                default: throw new Exception("Sort key not implemented!");
            }

            var comparison = new Comparison<PlayersRankingRow>(comparationFunc);
            data.Sort(comparison);
            return data;
        }
    }
}
