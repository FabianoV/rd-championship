﻿using RDChampionship.Models.Logic;
using RDChampionship.Repositories.Contract;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RDChampionship.Services.Implementation
{
    public class UserStatisticsService : IUserStatisticsService
    {
        // Services
        public IUserStatisticsRepository StatsRepository { get; set; }
        public IMatchRepository MatchRepository { get; set; }
        public ITournamentMatchRepository TournamentMatchRepository { get; set; }
        public ISetRepository SetRepository { get; set; }

        // Consrtuctor
        public UserStatisticsService(IUserStatisticsRepository statsRepository, IMatchRepository matchRepository, ITournamentMatchRepository tournamentMatchRepository)
        {
            StatsRepository = statsRepository;
            MatchRepository = matchRepository;
            TournamentMatchRepository = tournamentMatchRepository;
        }

        // Methods
        public void SumStatisticsForMatch(string userId, Guid matchId)
        {
            // Prepare data
            var match = MatchRepository.Get(matchId);

            if (match.Status != MatchStatus.Ended)
            {
                throw new Exception("Cannot summarize statistics from not ended match!");
            }

            var sets = SetRepository.GetSetsForMatch(matchId);
            var pointsScored = 0;
            var userTeam = TeamChoice.None;
            if (userId == match.TeamOnePlayerOneId || userId == match.TeamOnePlayerTwoId)
            {
                userTeam = TeamChoice.TeamOne;
                pointsScored += Convert.ToInt32(sets.Sum(s => s.TeamOnePoints));
            }
            else if (userId == match.TeamTwoPlayerOneId || userId == match.TeamTwoPlayerTwoId)
            {
                userTeam = TeamChoice.TeamTwo;
                pointsScored += Convert.ToInt32(sets.Sum(s => s.TeamTwoPoints));
            }

            // Sum statistics
            var userStats = StatsRepository.Get(userId);
            userStats.MatchesPlayed++;
            userStats.PointsScoredInTeam += pointsScored;
            StatsRepository.Update(userStats);
        }

        public void SumStatisticsForTournamentMatch(string userId, Guid tournamentMatchId)
        {
            // Prepare data
            var match = TournamentMatchRepository.Get(tournamentMatchId);

            if (match.Status != MatchStatus.Ended)
            {
                throw new Exception("Cannot summarize statistics from not ended match!");
            }

            var sets = SetRepository.GetSetsForMatch(tournamentMatchId);
            var pointsScored = 0;
            var userTeam = TeamChoice.None;
            if (userId == match.TeamOnePlayerOneId || userId == match.TeamOnePlayerTwoId)
            {
                userTeam = TeamChoice.TeamOne;
                pointsScored += Convert.ToInt32(sets.Sum(s => s.TeamOnePoints));
            }
            else if (userId == match.TeamTwoPlayerOneId || userId == match.TeamTwoPlayerTwoId)
            {
                userTeam = TeamChoice.TeamTwo;
                pointsScored += Convert.ToInt32(sets.Sum(s => s.TeamTwoPoints));
            }

            // Sum statistics
            var userStats = StatsRepository.Get(userId);
            userStats.MatchesPlayed++;
            userStats.PointsScoredInTeam += pointsScored;
            userStats.TournamentMatchesPlayed++;
            userStats.TournamentPointsScored += pointsScored;
            StatsRepository.Update(userStats);
        }
    }
}
