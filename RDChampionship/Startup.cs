﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RDChampionship.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RDChampionship.Services.Contract;
using RDChampionship.Services.Implementation;
using RDChampionship.Repositories.Contract;
using RDChampionship.Repositories.Implementation;
using Microsoft.AspNetCore.Authentication.OAuth;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using RDChampionship.Services.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace RDChampionship
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication().AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = Configuration["Authentication:Facebook:AppId"];
                facebookOptions.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                facebookOptions.Fields.Add("picture");
                facebookOptions.Events = new OAuthEvents
                {
                    OnCreatingTicket = context =>
                    {
                        var identity = (ClaimsIdentity)context.Principal.Identity;
                        var profileImg = context.User["picture"]["data"].Value<string>("url");
                        var facebookId = context.User.Value<string>("id");
                        identity.AddClaim(new Claim("facebook-picture-url", profileImg));
                        identity.AddClaim(new Claim("facebook-id", facebookId));
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<AppUser, AppRole>(options => {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 8;
            })
                .AddDefaultTokenProviders()
                .AddRoles<AppRole>()
                .AddUserStore<AppUserStore>()
                .AddRoleStore<AppRoleStore>()
                .AddRoleManager<AppRoleManager>()
                .AddUserManager<AppUserManager>()
                .AddSignInManager<AppSignInManager>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            // Services
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IAppRoleService, AppRoleService>();
            services.AddTransient<IGlobalUserService, GlobalUserService>();
            services.AddTransient<ITournamentMatchControlService, TournamentMatchControlService>();
            services.AddTransient<IUserStatisticsService, UserStatisticsService>();
            services.AddTransient<IPlayersRankingService, PlayerRankingService>();
            services.AddTransient<IRepositoriesService, RepositoriesService>();

            // Repositories
            services.AddTransient<ITournamentRepository, TournamentRepository>();
            services.AddTransient<ITeamRepository, TeamRepository>();
            services.AddTransient<IInvitationRepository, InvitationRepository>();
            services.AddTransient<IRegistrationRepository, RegistrationRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();
            services.AddTransient<IGroupTeamsRepository, ScheduleGroupTeamsRepository>();
            services.AddTransient<ITournamentMatchRepository, TournamentMatchRepository>();
            services.AddTransient<IMatchRepository, MatchRepository>();
            services.AddTransient<IPointsRepository, PointsRepository>();
            services.AddTransient<ISetRepository, SetRepository>();
            services.AddTransient<IUserStatisticsRepository, UserStatisticsRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            ConfigureApplication(serviceProvider).Wait();
        }

        // This method was created to check some custom conditions and do some custom configuration in app during server starting.
        public async Task ConfigureApplication(IServiceProvider serviceProvider)
        {
            var applicationRoleManager = serviceProvider.GetService<IAppRoleService>();
            await applicationRoleManager.EnsureAppRolesExists();

            var globalUserService = serviceProvider.GetService<IGlobalUserService>();
            await globalUserService.EnsureGlobalUserExists();
            await globalUserService.EnsureGlobalUserHasGlobalRoles();
        }
    }
}
